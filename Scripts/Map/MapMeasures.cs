using UnityEngine;

namespace Map
{
    public class MapMeasures
    {
        public static float HexEdgeLength = 7f;
        public static float HexInnerRadius = Mathf.Sqrt(3) * HexEdgeLength / 2;
        public static float HexOuterRaduis = HexEdgeLength;
        public static float DistanceBetweenHexes = HexInnerRadius * 2;
        public static float HexInnerCircleLength = 2 * Mathf.PI * HexInnerRadius;
        public static float HexOuterCircleLength = 2 * Mathf.PI * HexOuterRaduis;
        //public static float hexSegmentLength = 2 * Mathf.PI * hexInnerRadius / 6;
    }
}
