﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

namespace Map
{
    public class Map : MonoBehaviour
    {
        public List<GameObject> spawnPoints;
        //public Dictionary<GameObject, List<GameObject>> hexesRelations = new Dictionary<GameObject, List<GameObject>>();

        private void Awake()
        {
            //foreach (Transform child in transform)
            //{
            //    hexesRelations.Add(child.gameObject, child.GetComponent<Hex>().neighbors);
            //}
        }

        public List<Hex> GetHexList()
        {
            return transform.GetComponentsInChildren<Hex>().ToList();
        }
    }
}
