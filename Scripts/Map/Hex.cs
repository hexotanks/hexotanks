﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Map
{
    public class Hex : MonoBehaviour, ISelectable
    {
        public enum Types
        {
            Template,
            Ground,
            Water
        }

        public Types type;
        public int x;
        public int y;

        public bool canShoot;
        public bool canMove;

        public List<GameObject> neighbors;

        public void Select()
        {
            throw new System.NotImplementedException();
        }

        public void Deselect()
        {
            throw new System.NotImplementedException();
        }

        public Hex GetNeighborById(int id)
        {
            throw new System.Exception();
        }
    }
}
