﻿namespace BezierCurve
{
    public class BezierCurveSettings
    {
        // more steps - more precision
        public const int BezierCurveLengthPrecisionSteps = 20;
    }
}
