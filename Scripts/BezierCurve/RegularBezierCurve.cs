﻿using UnityEngine;
using System.Collections.Generic;

namespace BezierCurve
{
	public class RegularBezierCurve : BezierCurve
	{
		public RegularBezierCurve(List<Vector3> controlPoints)
		{
			_controlPoints = controlPoints;
		}

		public override Vector3 GetVelocity(float t)
		{
			t = Mathf.Clamp01(t);
			t = NormalizeT(t);
			int n = _controlPoints.Count - 1;

			Vector3 result = Vector3.zero;
			for (int i = 0; i <= n - 1; i++)
			{
				result += MathUtils.GetBernsteinBasisPolynomials(n - 1, i, t) * (_controlPoints[i + 1] - _controlPoints[i]);
			}

			return n * result;
		}

		public override float GetSignedCurvature(float t)
		{
			Vector2 vel = GetVelocity(t).ToVector2Y();
			Vector2 acc = GetAcceleration(t).ToVector2Y();

			return -(vel.x * acc.y - vel.y * acc.x) / Mathf.Pow(vel.x * vel.x + vel.y * vel.y, 3.0f / 2.0f);
		}

		protected override Vector3 GetPoint(float t)
		{
			int n = _controlPoints.Count - 1;

			Vector3 result = Vector3.zero;
			for (int i = 0; i <= n; i++)
			{
				result += MathUtils.GetBernsteinBasisPolynomials(n, i, t) * _controlPoints[i];
			}

			return result;
		}

		protected override Vector3 GetAcceleration(float t)
		{
			t = Mathf.Clamp01(t);
			t = NormalizeT(t);
			int n = _controlPoints.Count - 1;

			Vector3 result = Vector3.zero;
			for (int i = 0; i <= n - 2; i++)
			{
				result += MathUtils.GetBernsteinBasisPolynomials(n - 2, i, t) * (_controlPoints[i + 2] - _controlPoints[i + 1] - _controlPoints[i + 1] + _controlPoints[i]);
			}

			return n * (n - 1) * result;
		}
	}
}
