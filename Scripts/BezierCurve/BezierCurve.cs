﻿using System.Collections.Generic;
using UnityEngine;

namespace BezierCurve
{
    public abstract class BezierCurve
    {
        public float Length { get; set; } = 0.0f;

        protected List<Vector3> _controlPoints;
        protected List<float> _arcsLength = new List<float>();

        public Vector3 GetNormalizedPoint(float t)
        {
			t = Mathf.Clamp01(t);
			t = NormalizeT(t);
			return GetPoint(t);
		}

        public abstract Vector3 GetVelocity(float t);
        public abstract float GetSignedCurvature(float t);
		protected abstract Vector3 GetPoint(float t);
		protected abstract Vector3 GetAcceleration(float t);

		protected Vector3 GetRawPoint(float t)
		{
			t = Mathf.Clamp01(t);
			return GetPoint(t);
		}

		public BezierCurveIterator GetIterator(BezierCurve curve)
        {
			GetArcsLength();
			return new BezierCurveIterator(this);
        }

		protected float NormalizeT(float t)
		{
			float targetLength = Length * t;
			int index = _arcsLength.FindIndex(x => x >= targetLength);
			if (_arcsLength[index] > targetLength)
			{
				index--;
			}

			float lengthBefore = _arcsLength[index];

			if (lengthBefore == targetLength)
			{
				return t;
			}
			else
			{
				return (index + (targetLength - lengthBefore) / (_arcsLength[index + 1] - lengthBefore)) / (_arcsLength.Count - 1);
			}
		}

		protected void GetArcsLength(int bezierCurveLengthPrecisionSteps = BezierCurveSettings.BezierCurveLengthPrecisionSteps)
		{
			float precision = 1.0f / (bezierCurveLengthPrecisionSteps * _controlPoints.Count);
			_arcsLength.Add(0);
			for (var i = precision; i < 1.0f; i += precision)
			{
				float arcLength = Vector3.Distance(GetRawPoint(i - precision), GetRawPoint(i));
				Length += arcLength;
				_arcsLength.Add(Length);
			}
		}
	}
}
