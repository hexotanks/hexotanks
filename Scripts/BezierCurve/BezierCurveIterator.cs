﻿using UnityEngine;

namespace BezierCurve
{
    public class BezierCurveIterator
    {
        private BezierCurve _curve;
        private float _currentPosition = 0.0f;
        private Vector3 _currentPoint;

        public BezierCurveIterator(BezierCurve curve)
        {
            _curve = curve;
            _currentPoint = curve.GetNormalizedPoint(_currentPosition);
        }

        public Vector3 GetPoint(float distance)
        {
            float shift = distance / _curve.Length;
            float newPosition = _currentPosition + shift;
            _currentPosition = Mathf.Clamp01(newPosition);
            _currentPoint = _curve.GetNormalizedPoint(newPosition);
            return _currentPoint;
        }

        public float GetSignedCurvature()
        {
            return _curve.GetSignedCurvature(_currentPosition);
        }

        public Vector3 GetVelocity()
        {
            return _curve.GetVelocity(_currentPosition).normalized + _currentPoint;
        }

        public bool IsEnd()
        {
            return _currentPosition == 1.0f;
        }
    }
}
