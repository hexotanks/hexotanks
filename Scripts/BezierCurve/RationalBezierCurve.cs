﻿using UnityEngine;
using System.Collections.Generic;

namespace BezierCurve
{
	public class RationalBezierCurve : BezierCurve
	{
		private List<float> _controlPointsRatios = new List<float>();

		public RationalBezierCurve(List<Vector3> controlPoints, List<float> controlPointsRatios)
		{
			_controlPoints = controlPoints;
			_controlPointsRatios = controlPointsRatios;
		}

		public override Vector3 GetVelocity(float t)
		{
			t = Mathf.Clamp01(t);
			t = NormalizeT(t);

			int n = _controlPoints.Count - 1;

			return n * GetWeight(0, n - 1, t) * GetWeight(1, n - 1, t) / (Mathf.Pow(GetWeight(0, n, t), 2)) * (GetPoint(1, n - 1, t) - GetPoint(0, n - 1, t));
		}

		public override float GetSignedCurvature(float t)
		{
			Vector2 vel = GetVelocity(t).ToVector2Y();
			Vector2 acc = GetAcceleration(t).ToVector2Y();

			return -(acc.y * vel.x - acc.x * vel.y) / Mathf.Pow(vel.x * vel.x + vel.y * vel.y, 3.0f / 2.0f);
		}

		protected override Vector3 GetPoint(float t)
		{
			int n = _controlPoints.Count - 1;

			Vector3 numerator = Vector3.zero;
			float denominator = 0.0f;
			for (int i = 0; i <= n; i++)
			{
				float bernsteinBasisPolynomials = MathUtils.GetBernsteinBasisPolynomials(n, i, t);
				numerator += bernsteinBasisPolynomials * _controlPoints[i] * _controlPointsRatios[i];
				denominator += bernsteinBasisPolynomials * _controlPointsRatios[i];
			}

			return numerator / denominator;
		}

		protected override Vector3 GetAcceleration(float t)
		{
			t = Mathf.Clamp01(t);
			t = NormalizeT(t);
			int n = _controlPoints.Count - 1;

			float weight_2_n_2 = GetWeight(2, n - 2, t);
			float weight_0_n = GetWeight(0, n, t);
			float weight_0_n_2 = GetWeight(0, n - 2, t);
			float weight_0_n_1 = GetWeight(0, n - 1, t);
			float weight_1_n_1 = GetWeight(1, n - 1, t);

			Vector3 part1 = n * weight_2_n_2 / (Mathf.Pow(weight_0_n, 3)) * (2 * n * Mathf.Pow(weight_0_n_1, 2) - (n - 1) * weight_0_n_2 * weight_0_n - 2 * weight_0_n_1 * weight_0_n) * (GetPoint(2, n - 2, t) - GetPoint(1, n - 2, t));
			Vector3 part2 = n * weight_0_n_2 / (Mathf.Pow(weight_0_n, 3)) * (2 * n * Mathf.Pow(weight_1_n_1, 2) - (n - 1) * weight_2_n_2 * weight_0_n - 2 * weight_1_n_1 * weight_0_n) * (GetPoint(1, n - 2, t) - GetPoint(0, n - 2, t));

			return part1 - part2;
		}

		private Vector3 GetPoint(int i, int k, float t)
		{
			Vector3 numerator = Vector3.zero;
			float denominator = 0.0f;
			for (int j = 0; j <= k; j++)
			{
				float bernsteinBasisPolynomials = MathUtils.GetBernsteinBasisPolynomials(k, j, t);
				numerator += bernsteinBasisPolynomials * _controlPoints[i + j] * _controlPointsRatios[i + j];
				denominator += bernsteinBasisPolynomials * _controlPointsRatios[i + j];
			}

			return numerator / denominator;
		}

		private float GetWeight(int i, int k, float t)
		{
			float result = 0.0f;
			for (int j = 0; j <= k; j++)
			{
				result += MathUtils.GetBernsteinBasisPolynomials(k, j, t) * _controlPointsRatios[i + j];
			}

			return result;
		}
	}
}
