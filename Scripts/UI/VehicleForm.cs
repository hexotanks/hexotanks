﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class VehicleForm : Form
{
    [SerializeField]
    private Button moveBtn;
    private Vehicle selectedVehicle;

    private void Start()
    {
        moveBtn.onClick.AddListener(ShowMoveHexesOnClick);
    }

    public void Show(Vehicle vehicle)
    {
        selectedVehicle = vehicle;
        Activate();
    }

    public void Hide()
    {
        selectedVehicle = null;
        Deactivate();
    }

    void ShowMoveHexesOnClick()
    {
        
    }
}
