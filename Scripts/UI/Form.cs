﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Form : MonoBehaviour
{
    protected void Activate()
    {
        gameObject.SetActive(true);
    }

    protected void Deactivate()
    {
        gameObject.SetActive(false);
    }
}
