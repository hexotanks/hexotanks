﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class GameObjectNameManager
{
    private static Dictionary<string, int> namesDict = new Dictionary<string, int>();

    public static string AddName(string oldName)
    {
        if (oldName.Contains("(Clone)"))
        {
            string logicName = oldName.Substring(0, oldName.LastIndexOf("(Clone)"));
            if (!namesDict.ContainsKey(logicName))
            {
                namesDict.Add(logicName, 0);
            }

            int newInd = namesDict[logicName] + 1;
            string newName = $"{logicName}_{newInd}";
            namesDict[logicName] = newInd;
            return newName;
        }

        return oldName;
        
        //string pattern = logicName + @"(\d+)";
        //int maxNum = names.FindAll(n => RegexUtils.Contains(n, pattern)).Select(n => int.Parse(RegexUtils.GetGroupsValues(n, pattern)[0])).Max();
    }
}
