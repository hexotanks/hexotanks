﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapManager : Manager
{
    private Map map;

    public void SetMap(GameObject map)
    {
        GameObject instMap = ObjectUtils.Instantiate(map);
        this.map = instMap.GetComponent<Map>();
    }

    public Vector3 GetSpawnPoint()
    {
        return map.spawnPoints[0].transform.position;
    }

    public List<Hex> GetMapHexList()
    {
        return map.GetHexList();
    }
}
