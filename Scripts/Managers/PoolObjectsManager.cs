﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Helper;

public class PoolObjectsManager : SingletonBehaviour<PoolObjectsManager>
{
    private Vector3 poolCenter = new Vector3(-1000.0f, -1000.0f, -1000.0f);

    private List<GameObject> availableGO = new List<GameObject>();

    public GameObject Get(GameObject prefab)
    {
        GameObject resultGO = availableGO.Find(go => go.name.Contains(prefab.name));
        if (resultGO == null)
        {
            resultGO = Instantiate(prefab);
            resultGO.transform.position = poolCenter;
            resultGO.SetActive(false);
        }
        return resultGO;
    }
}
