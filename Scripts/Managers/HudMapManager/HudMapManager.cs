﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HudMapManager : Manager
{
    [SerializeField]
    private LineRenderer hexHighlightLine;
    [SerializeField]
    private MapManager mapManager;
    [SerializeField]
    private GameObject straightArrow;

    private HudMapMode mode = HudMapMode.None;

    public void HighlightHexes(List<Hex> hexes)
    {
        List<Mesh> hexMeshes = new List<Mesh>();
        List<Transform> hexTransforms = new List<Transform>();
        foreach (Hex hex in hexes)
        {
            hexMeshes.Add(hex.GetComponent<MeshFilter>().mesh);
            hexTransforms.Add(hex.transform);
        }
        List<Vector3> boundary = MeshUtils.GetBoundaryVertices(hexMeshes, true, hexTransforms);
        for (int i = 0; i < boundary.Count; i++)
        {
            boundary[i] = boundary[i].ToWorldPosition(transform);
            boundary[i] = new Vector3(boundary[i].x, boundary[i].y + 0.1f, boundary[i].z);
        }

        hexHighlightLine.positionCount = boundary.Count;
        hexHighlightLine.SetPositions(boundary.ToArray());
        hexHighlightLine.gameObject.SetActive(true);
    }

    public void ClearHighlightHexes()
    {
        hexHighlightLine.gameObject.SetActive(false);
    }

    public void ShowMoveHexes(Hex fromHex, List<Hex> toHexes)
    {

    }

    public void HideMoveHexes()
    {

    }
}
