﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VehiclesManager : Manager
{
    private Vehicle vehicle;

    public void SetVehicle(GameObject vehicle, Vector3 spawnPoint)
    {
        GameObject instVehicle = Instantiate(vehicle, spawnPoint, Quaternion.identity);

        this.vehicle = instVehicle.GetComponent<Vehicle>();
    }
}
