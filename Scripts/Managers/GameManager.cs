﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public GameObject map;
    public GameObject vehicle;

    [SerializeField]
    private MapManager mapManager;
    [SerializeField]
    private VehiclesManager vehiclesManager;
    [SerializeField]
    private SelectionManager selectionManager;
    [SerializeField]
    private HudMapManager hudMapManager;
    [SerializeField]
    private GuiManager guiManager;

    private void Start()
    {
        mapManager.SetMap(map);
        Vector3 spawnPoint = mapManager.GetSpawnPoint();

        vehiclesManager.SetVehicle(vehicle, spawnPoint);
    }
}
