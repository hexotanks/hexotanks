﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GuiManager : Manager
{
    [SerializeField]
    private VehicleForm vehicleForm;

    private void Start()
    {
        
    }

    public void ShowVehicleForm(Vehicle vehicle)
    {
        vehicleForm.Show(vehicle);
    }

    public void HideAllForms()
    {
        vehicleForm.Hide();
    }
}
