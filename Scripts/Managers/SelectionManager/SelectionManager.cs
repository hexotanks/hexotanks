﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelectionManager : Manager
{
    private Camera camera;
    [SerializeField]
    private MapManager mapManager;
    [SerializeField]
    private HudMapManager hudMapManager;
    [SerializeField]
    private GuiManager guiManager;

    private SelectionMode currentSelection = SelectionMode.None;
    private Vehicle selectedVehicle;
    private Hex selectedHex;

    private void Start()
    {
        camera = GetComponent<Camera>();
    }

    private void Update()
    {
        camera.transform.Translate(Input.GetAxis("Horizontal"), -Input.GetAxis("MouseScrollWheel") * 30.0f, Input.GetAxis("Vertical"), Space.World);

        if (Input.GetMouseButtonDown(0))
        {
            RaycastHit hitInfo = new RaycastHit();
            bool hit = Physics.Raycast(camera.ScreenPointToRay(Input.mousePosition), out hitInfo);
            if (hit)
            {
                if (hitInfo.transform.GetComponent<Vehicle>())
                {
                    SelectVehicle(hitInfo.transform.GetComponent<Vehicle>());
                }
                else if (hitInfo.transform.GetComponent<Hex>())
                {
                    SelectHex(hitInfo.transform.GetComponent<Hex>());
                }
            }
        }

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            ResetSelection();
        }
    }

    private void FixedUpdate()
    {
        
    }

    private void SelectVehicle(Vehicle vehicle)
    {
        switch (currentSelection)
        {
            case SelectionMode.None:
            {
                selectedVehicle = vehicle;
                currentSelection = SelectionMode.TankSelected;
                guiManager.ShowVehicleForm(vehicle);
                break;
            }
            case SelectionMode.TankSelected:
            {
                ResetSelection();
                selectedVehicle = vehicle;
                currentSelection = SelectionMode.TankSelected;
                guiManager.ShowVehicleForm(vehicle);
                break;
            }
            case SelectionMode.HexSelected:
            {
                ResetSelection();
                selectedVehicle = vehicle;
                currentSelection = SelectionMode.TankSelected;
                guiManager.ShowVehicleForm(vehicle);
                break;
            }
        }
    }

    private void SelectHex(Hex hex)
    {
        switch (currentSelection)
        {
            case SelectionMode.None:
            {
                selectedHex = hex;
                currentSelection = SelectionMode.HexSelected;
                //Test
                List<Hex> hexes = new List<Hex>();
                hexes.Add(hex);
                //List<GameObject> neighbors = hex.neighbors;
                //foreach (GameObject go in neighbors)
                //{
                //    if (go.GetComponent<Hex>() != null)
                //    {
                //        hexes.Add(go.GetComponent<Hex>());
                //    }
                //}

                hudMapManager.HighlightHexes(hexes);
                break;
            }
            case SelectionMode.TankSelected:
            {
                ResetSelection();
                selectedHex = hex;
                currentSelection = SelectionMode.HexSelected;

                //Test
                List<Hex> hexes = new List<Hex>();
                hexes.Add(hex);
                //List<GameObject> neighbors = hex.neighbors;
                //foreach (GameObject go in neighbors)
                //{
                //    if (go.GetComponent<Hex>() != null)
                //    {
                //        hexes.Add(go.GetComponent<Hex>());
                //    }
                //}

                hudMapManager.HighlightHexes(hexes);
                break;
            }
            case SelectionMode.HexSelected:
            {
                if (hex != selectedHex)
                {
                    ResetSelection();
                    selectedHex = hex;
                    currentSelection = SelectionMode.HexSelected;

                    //Test
                    List<Hex> hexes = new List<Hex>();
                    hexes.Add(hex);
                    //List<GameObject> neighbors = hex.neighbors;
                    //foreach (GameObject go in neighbors)
                    //{
                    //    if (go != null && go.GetComponent<Hex>() != null)
                    //    {
                    //        hexes.Add(go.GetComponent<Hex>());
                    //    }
                    //}

                    hudMapManager.HighlightHexes(hexes);
                }
                break;
            }
        }
    }

    private void ResetSelection()
    {
        selectedHex = null;
        selectedVehicle = null;
        currentSelection = SelectionMode.None;
        guiManager.HideAllForms();
        hudMapManager.ClearHighlightHexes();
    }
}
