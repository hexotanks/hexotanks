﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HexUtils
{
    private static int hexSegmentsCount = 6;

    public static int GetNextHexSegmentIndex(int currentSegmentIndex)
    {
        return (currentSegmentIndex + 1) == hexSegmentsCount ? 0 : (currentSegmentIndex + 1);
    }

    public static int GetPrevHexSegmentIndex(int currentSegmentIndex)
    {
        return (currentSegmentIndex - 1) == -1 ? hexSegmentsCount - 1 : (currentSegmentIndex - 1);
    }

    public static int GetHexSegmentIndexByAngle(float angle)
    {
        angle = (angle > 360.0f) ? 0.0f : angle;
        return (int)(angle / 60.0f);
    }

    public static float GetAngleByHexSegmentIndex(int hexIndex)
    {
        return hexIndex * 60.0f;
    }
}
