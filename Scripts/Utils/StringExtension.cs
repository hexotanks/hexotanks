﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class StringExtension
{
    public static string CapitalizeFirstLetter(this string str)
    {
        return char.ToUpper(str[0]) + str.Substring(1);
    }
}
