﻿using UnityEngine;

public class SpecParser
{
    private string stringData;

    public SpecParser(string stringData)
    {
        this.stringData = stringData;
    }

    public TankTrackLinkData GetTankTrackLinkData()
    {
        TankTrackLinkData data = new TankTrackLinkData();

        string[] splitData = stringData.Split(' ');

        float x = float.Parse(splitData[1]);
        float y = float.Parse(splitData[2]);
        float z = float.Parse(splitData[3]);
        data.linkCenter = new Vector3(x, y, z);
        data.v1 = int.Parse(splitData[4]);
        data.v2 = int.Parse(splitData[5]);
        data.angleX = float.Parse(splitData[6]);

        return data;
    }

    public int GetTankTrackLinksVerticesCount()
    {
        string[] splitData = stringData.Split(' ');

        return int.Parse(splitData[1]);
    }

    public TankWheelsData GetTankWheelsData()
    {
        TankWheelsData data = new TankWheelsData();

        string[] splitData = stringData.Split(' ');

        data.verticesCount = int.Parse(splitData[1]);
        float x = float.Parse(splitData[2]);
        float y = float.Parse(splitData[3]);
        float z = float.Parse(splitData[4]);
        data.wheelCenter = new Vector3(x, y, z);
        data.radius = float.Parse(splitData[5]);

        return data;
    }

    public TankData GetTankData()
    {
        TankData data = new TankData();

        string[] splitData = stringData.Split(' ');

        float x = float.Parse(splitData[1]);
        float y = float.Parse(splitData[2]);
        float z = float.Parse(splitData[3]);
        data.shootPoint = new Vector3(x, y, z);
        data.caliber = splitData[4];
        data.nation = splitData[8];

        return data;
    }
}
