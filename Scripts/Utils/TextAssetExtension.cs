﻿using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class TextAssetExtension
{
    public static List<string> ReadLines(this TextAsset textAsset)
    {
        string text = textAsset.text;
        return text.Split(new string[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries).ToList();
    }
}
