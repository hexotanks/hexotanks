﻿using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DebugUtils
{
    public static void DrawCross(Vector3 position, Color color, float duration)
    {
        Debug.DrawLine(position.AddValueToX(-0.2f), position.AddValueToX(0.2f), color, duration);
        Debug.DrawLine(position.AddValueToY(-0.2f), position.AddValueToY(0.2f), color, duration);
        Debug.DrawLine(position.AddValueToZ(-0.2f), position.AddValueToZ(0.2f), color, duration);
    }

    public static void LogVector(Vector3 v, int precision = 4)
    {
        Debug.Log(v.ToString($"F{precision}"));
    }

    public static void LogSeparator()
    {
        Debug.Log("----------");
    }
}
