﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class MeshUtils
{
    public static List<Vector3> GetBoundaryVertices(List<Mesh> meshes, bool toWorldCoord = true, List<Transform> transforms = null)
    {
        List<MeshTriangle> triangles = new List<MeshTriangle>();
        for (int i = 0; i < meshes.Count; i++)
        {
            if (toWorldCoord)
            {
                triangles.AddRange(meshes[i].GetTriangles(toWorldCoord, transforms[i]));
            }
            else
            {
                triangles.AddRange(meshes[i].GetTriangles(false, null));
            }
        }

        List<MeshEdge> allEdges = new List<MeshEdge>();
        foreach (MeshTriangle triangle in triangles)
        {
            allEdges.AddRange(triangle.edges);
        }
        List<MeshEdge> adjacentEdges = new List<MeshEdge>();
        foreach (MeshTriangle triangle in triangles)
        {
            foreach (MeshTriangle otherTriangle in triangles)
            {
                if (otherTriangle == triangle) continue;
                adjacentEdges.Add(triangle.GetAdjacentEdge(otherTriangle));
            }
        }

        List<MeshEdge> boundaryEdges = allEdges.Except(adjacentEdges).ToList();

        List<Vector3> boundaryPoints = new List<Vector3>();
        boundaryPoints.Add(boundaryEdges[0].points[0]);
        Vector3 lastBoundaryPoint = boundaryEdges[0].points[1];
        boundaryPoints.Add(lastBoundaryPoint);
        boundaryEdges.Remove(boundaryEdges[0]);
        while (boundaryEdges.Count > 0)
        {
            MeshEdge nextEdge = boundaryEdges.Find(edge => edge.Contains(lastBoundaryPoint));
            int pointInd = nextEdge.GetPointIndex(lastBoundaryPoint);

            boundaryPoints.Add(nextEdge.points[pointInd]);
            lastBoundaryPoint = nextEdge.GetNextPoint(pointInd);
            boundaryPoints.Add(lastBoundaryPoint);
            boundaryEdges.Remove(nextEdge);
        }

        return boundaryPoints;
    }
}
