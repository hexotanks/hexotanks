﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeshTriangle
{
    public MeshEdge[] edges = new MeshEdge[3];
    public Vector3[] points = new Vector3[3];

    public MeshTriangle(Vector3 p1, Vector3 p2, Vector3 p3)
    {
        points[0] = p1;
        points[1] = p2;
        points[2] = p3;

        MeshEdge e1 = new MeshEdge(p1, p2);
        MeshEdge e2 = new MeshEdge(p2, p3);
        MeshEdge e3 = new MeshEdge(p3, p1);

        edges[0] = e1;
        edges[1] = e2;
        edges[2] = e3;
    }

    // смежная грань
    public MeshEdge GetAdjacentEdge(MeshTriangle other)
    {
        foreach (MeshEdge edge in edges)
        {
            foreach (MeshEdge otherEdge in other.edges)
            {
                if (edge.Equals(otherEdge))
                {
                    return edge;
                }
            }
        }

        return null;
    }
}
