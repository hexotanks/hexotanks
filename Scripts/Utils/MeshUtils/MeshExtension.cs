﻿using System.Collections;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;

public static class MeshExtension
{
    /// <summary>
    /// Get vertices position
    /// </summary>
    /// <param name="mesh"></param>
    /// <param name="triangleFromInd"></param>
    /// <param name="count"></param>
    /// <returns></returns>
    public static List<Vector3> GetTrianglesVerticesPosition(this Mesh mesh, int triangleFromInd, int count)
    {
        // each triangle contains 3 vertices
        List<int> triangles = new List<int>(mesh.triangles).GetRange(triangleFromInd * 3, count * 3);
        List<Vector3> vertices = new List<Vector3>(mesh.vertices);
        int min = triangles.Min();
        int max = triangles.Max();
        return vertices.GetRange(min, (max - min) + 1);
    }

    /// <summary>
    /// Get vertices index
    /// </summary>
    /// <param name="mesh"></param>
    /// <param name="triangleFromInd"></param>
    /// <param name="count"></param>
    /// <returns></returns>
    public static List<int> GetTrianglesVertices2(this Mesh mesh, int triangleFromInd, int count)
    {
        // each triangle contains 3 vertices
        return new List<int>(mesh.triangles).GetRange(triangleFromInd * 3, count * 3);
    }

    /// <summary>
    /// Get boundary for 2D mesh
    /// </summary>
    /// <param name="mesh"></param>
    /// <returns></returns>
    public static List<Vector3> GetBoundaryVertices(this Mesh mesh, bool toWorldCoord = true, Transform transform = null)
    {
        List<MeshTriangle> triangles = mesh.GetTriangles(toWorldCoord, transform);
        List<MeshEdge> allEdges = new List<MeshEdge>();
        foreach (MeshTriangle triangle in triangles)
        {
            allEdges.AddRange(triangle.edges);
        }
        List<MeshEdge> adjacentEdges = new List<MeshEdge>();
        foreach (MeshTriangle triangle in triangles)
        {
            foreach (MeshTriangle otherTriangle in triangles)
            {
                if (otherTriangle == triangle) continue;
                adjacentEdges.Add(triangle.GetAdjacentEdge(otherTriangle));
            }
        }

        List<MeshEdge> boundaryEdges = allEdges.Except(adjacentEdges).ToList();

        List<Vector3> boundaryPoints = new List<Vector3>();
        boundaryPoints.Add(boundaryEdges[0].points[0]);
        Vector3 lastBoundaryPoint = boundaryEdges[0].points[1];
        boundaryPoints.Add(lastBoundaryPoint);
        boundaryEdges.Remove(boundaryEdges[0]);
        while (boundaryEdges.Count > 0)
        {
            MeshEdge nextEdge = boundaryEdges.Find(edge => edge.Contains(lastBoundaryPoint));
            int pointInd = nextEdge.GetPointIndex(lastBoundaryPoint);

            boundaryPoints.Add(nextEdge.points[pointInd]);
            lastBoundaryPoint = nextEdge.GetNextPoint(pointInd);
            boundaryPoints.Add(lastBoundaryPoint);
            boundaryEdges.Remove(nextEdge);
        }

        return boundaryPoints;
    }

    public static List<MeshTriangle> GetTriangles(this Mesh mesh, bool toWorldCoord = true, Transform transform = null)
    {
        int[] triangles = mesh.triangles;
        Vector3[] vertices = mesh.vertices;

        List<MeshTriangle> result = new List<MeshTriangle>();
        for (int i = 0; i < triangles.Length; i += 3)
        {
            Vector3 p1 = Vector3.zero;
            Vector3 p2 = Vector3.zero;
            Vector3 p3 = Vector3.zero;
            if (toWorldCoord)
            {
                p1 = vertices[triangles[i]].ToWorldPosition(transform);
                p2 = vertices[triangles[i + 1]].ToWorldPosition(transform);
                p3 = vertices[triangles[i + 2]].ToWorldPosition(transform);
            }
            else
            {
                p1 = vertices[triangles[i]];
                p2 = vertices[triangles[i + 1]];
                p3 = vertices[triangles[i + 2]];
            }
            result.Add(new MeshTriangle(p1, p2, p3));
        }
        return result;
    }
}
