﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeshEdge
{
    public Vector3[] points = new Vector3[2];

    public MeshEdge(Vector3 p1, Vector3 p2)
    {
        points[0] = p1;
        points[1] = p2;
    }

    public bool Equals(MeshEdge other)
    {
        if (other == null)
            return false;

        return ((this.points[0] == other.points[0]) && (this.points[1] == other.points[1])) || ((this.points[0] == other.points[1]) && (this.points[1] == other.points[0]));
    }

    public bool Contains(Vector3 point)
    {
        return points[0] == point || points[1] == point;
    }

    public int GetPointIndex(Vector3 point)
    {
        return points[0] == point ? 0 : points[1] == point ? 1 : -1;
    }

    public Vector3 GetNextPoint(int lastPointIndex)
    {
        return lastPointIndex == 0 ? points[1] : points[0];
    }
}
