﻿using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Vector3Extension
{
    public static Vector3 AddValueToX(this Vector3 vector, float value)
    {
        return new Vector3(vector.x + value, vector.y, vector.z);
    }

    public static Vector3 AddValueToY(this Vector3 vector, float value)
    {
        return new Vector3(vector.x, vector.y + value, vector.z);
    }

    public static Vector3 AddValueToZ(this Vector3 vector, float value)
    {
        return new Vector3(vector.x, vector.y, vector.z + value);
    }

    public static Vector3 Round(this Vector3 vector, int digits)
    {
        return new Vector3(vector.x.Round(digits), vector.y.Round(digits), vector.z.Round(digits));
    }

    public static Vector2 ToVector2Y(this Vector3 vector)
    {
        return new Vector2(vector.x, vector.z);
    }

    public static Vector3 ToWorldPosition(this Vector3 vector, Transform transform)
    {
        Matrix4x4 localToWorld = transform.localToWorldMatrix;
        return localToWorld.MultiplyPoint3x4(vector);
    }
}
