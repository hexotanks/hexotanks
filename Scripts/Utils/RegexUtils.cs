﻿using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;

public class RegexUtils
{
    public static List<string> GetGroupsValues(string targetString, string pattern)
    {
        List<string> resultStrings = new List<string>();

        Regex regex = new Regex(pattern, RegexOptions.IgnoreCase);
        Match m = regex.Match(targetString);
        while (m.Success)
        {
            for (int i = 1; i <= m.Groups.Count; i++)
            {
                Group g = m.Groups[i];
                resultStrings.Add(g.Value);
            }
            m = m.NextMatch();
        }

        return resultStrings;
    }

    public static string Replace(string targetString, string pattern, string replacement)
    {
        return Regex.Replace(targetString, pattern, replacement, RegexOptions.Multiline);
    }

    public static bool Contains(string targetString, string pattern)
    {
        Regex regex = new Regex(pattern);
        Match match = regex.Match(targetString);
        return match.Success;
    }
}
