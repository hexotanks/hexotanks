﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomUtils
{
    public static bool RandomChance(float chance)
    {
        float randomValue = Random.value;
        return randomValue < chance;
    }
}
