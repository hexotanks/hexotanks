﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AdvancedRandomChance
{
    private List<bool> initChances = new List<bool>();
    private List<bool> currentChances = new List<bool>();

    public AdvancedRandomChance(float chance)
    {
        int trueChancesCount = (int)(chance * 100);
        int falseChancesCount = 100 - trueChancesCount;
        
        for (int i = 0; i < falseChancesCount; i++)
        {
            initChances.Add(false);
        }

        for (int i = 0; i < trueChancesCount; i++)
        {
            initChances.Add(true);
        }

        Restart();
    }

    private void Restart()
    {
        currentChances = initChances.GetRange(0, 100);
    }

    public bool Calc()
    {
        int randomIndex = Random.Range(0, currentChances.Count);

        bool result = currentChances[randomIndex];
        currentChances.RemoveAt(randomIndex);

        if (currentChances.Count == 0)
        {
            Restart();
        }

        return result;
    }

    public AdvancedRandomChanceState GetState()
    {
        AdvancedRandomChanceState result = new AdvancedRandomChanceState();

        foreach (bool currentChance in currentChances)
        {
            if (currentChance)
            {
                result.trueChancesCount += 1;
            }
            else
            {
                result.falseChancesCount += 1;
            }
        }

        return result;
    }

    public void SetState(AdvancedRandomChanceState state)
    {
        currentChances = new List<bool>();

        for (int i = 0; i < state.falseChancesCount; i++)
        {
            currentChances.Add(false);
        }

        for (int i = 0; i < state.trueChancesCount; i++)
        {
            currentChances.Add(true);
        }
    }
}
