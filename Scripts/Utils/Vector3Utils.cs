﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Vector3Utils
{
    public static Vector3 YNormal(Vector3 v1, Vector3 v2, bool clockwise = true)
    {
        Vector3 temp = v1 - v2;
        if (clockwise)
        {
            temp = new Vector3(-temp.z, 0.0f, temp.x);
            return temp.normalized;
        }
        else
        {
            temp = new Vector3(temp.z, 0.0f, -temp.x);
            return temp.normalized;
        }
    }
}
