﻿using UnityEngine;

public class TankTrackLinkData
{
    public Vector3 linkCenter;
    public int v1;
    public int v2;
    public float angleX;
}
