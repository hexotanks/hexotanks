﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Linq;
using BezierCurve;

public class Tank : Vehicle
{
    private TankBody body;
    private TankTurret turret;
    private TankTrack trackL;
    private TankTrack trackR;
    private TankWheels wheelsL;
    private TankWheels wheelsR;

    private TankBodyStates currentBodyState = TankBodyStates.Idle;

    private bool doActions = false;

    public float maxForwardSpeed;
    public float tankGroundedTrackLength;
    public float tankGroundedTrackWidth;
    private TankMovementCalculation tankMovementCalc;
    //private Vector3 positionBeforeMoving;
    //private Vector3 rotationBeforeMoving;

    public delegate void FinishDoAllActions();
    public event FinishDoAllActions OnFinishDoAllActions;

    private List<TankBodyActionTypes> bodyActions = new List<TankBodyActionTypes>();

    private Hex currentHex;

    private RegularBezierCurve curve;

    // for test
    //public List<GameObject> hexes;
    //public List<float> hexesRatios;
    //private List<Vector3> hexesPoints;
    //private RegularBezierCurve curve;
    //private BezierCurveIterator curveIterator;
    //private Vector3 prevForward = Vector3.forward;
    //private float angleSum = 0.0f;

    private void Start()
    {
        body = transform.Find("Body").GetComponent<TankBody>();
        turret = transform.Find("Turret").GetComponent<TankTurret>();
        trackL = transform.Find("Track_L").GetComponent<TankTrack>();
        trackR = transform.Find("Track_R").GetComponent<TankTrack>();
        wheelsL = transform.Find("Wheels_L").GetComponent<TankWheels>();
        wheelsR = transform.Find("Wheels_R").GetComponent<TankWheels>();

        tankMovementCalc = new TankMovementCalculation(maxForwardSpeed);

        //positionBeforeMoving = transform.position;
        //rotationBeforeMoving = transform.forward;

        // for test
        //hexesPoints = new List<Vector3>();
        //foreach (var hex in hexes)
        //{
        //    hexesPoints.Add(hex.transform.position);
        //}
        //curve = new RationalBezierCurve(hexesPoints, hexesRatios);
        //curveIterator = new BezierCurveIterator(curve);
    }

    public void AddBodyAction(TankBodyActionTypes type)
    {
        bodyActions.Add(type);
    }

    public void StartActions()
    {
        doActions = true;
    }

    private void StopActions()
    {
        doActions = false;
        OnFinishDoAllActions();
    }

    //private bool StartForwardMoving()
    //{
    //    currentBodyState = TankBodyStates.StartForwardMoving;

    //    float step = tankMovementCalc.GetDistanceStartForward();
    //    Vector3 targetPosition = positionBeforeMoving + transform.forward * MapMeasures.distanceBetweenHexes;
    //    transform.position = Vector3.MoveTowards(transform.position, targetPosition, step);

    //    if (Vector3.Distance(transform.position, targetPosition) <= 0.001f)
    //    {
    //        transform.position = targetPosition.Round(4);
    //        positionBeforeMoving = transform.position;
    //        tankMovementCalc.SetElapsedTimeToForwardMax();
    //        currentBodyState = TankBodyStates.ForwardMoving;
    //        return true;
    //    }

    //    ForwardMovingAnimation();

    //    return false;
    //}

    private bool ForwardMoving()
    {
        return false;
        //currentBodyState = TankBodyStates.ForwardMoving;

        //float curvature = curveIterator.GetCurvature();
        //tankMovementCalc.Forward(curvature);

        //float velocity = tankMovementCalc.GetVelocity();
        //float distance = velocity * Time.deltaTime;
        //Vector3 pos = curveIterator.GetPoint(distance);
        //Vector3 curveVelocity = curveIterator.GetVelocity();

        //transform.position = pos;
        //transform.LookAt(curveVelocity);

        //return curveIterator.IsEnd();
    }

    //private bool EndForwardMoving()
    //{
    //    currentBodyState = TankBodyStates.EndForwardMoving;

    //    float step = tankMovementCalc.GetDistanceEndForward();
    //    Vector3 targetPosition = positionBeforeMoving + transform.forward * MapMeasures.distanceBetweenHexes;
    //    transform.position = Vector3.MoveTowards(transform.position, targetPosition, step);

    //    if (Vector3.Distance(transform.position, targetPosition) <= 0.001f)
    //    {
    //        transform.position = targetPosition.Round(4);
    //        positionBeforeMoving = transform.position;
    //        currentBodyState = TankBodyStates.Idle;
    //        tankMovementCalc.ResetCalculation();
    //        return true;
    //    }

    //    ForwardMovingAnimation();

    //    return false;
    //}

    private void ForwardMovingAnimation()
    {
        bool isBodyMoves = bodyActions.Count > 0;

        float velocity = tankMovementCalc.GetVelocity();
        float leftTrackVelocity = tankMovementCalc.GetLeftTrackVelocity();
        float rightTrackVelocity = tankMovementCalc.GetRightTrackVelocity();

        Debug.Log("Velocity: " + velocity);
        Debug.Log("Left Velocity: " + leftTrackVelocity);
        Debug.Log("Right Velocity: " + rightTrackVelocity);

        trackL.Moving(leftTrackVelocity, isBodyMoves);
        trackR.Moving(rightTrackVelocity, isBodyMoves);
        wheelsL.Moving(leftTrackVelocity);
        wheelsR.Moving(rightTrackVelocity);
        body.Moving(velocity, isBodyMoves);
    }

    //private bool StartBackMoving()
    //{
    //    currentBodyState = TankBodyStates.StartBackMoving;

    //    float step = tankMovementCalc.GetDistanceStartBack();
    //    Vector3 targetPosition = positionBeforeMoving - transform.forward * MapMeasures.distanceBetweenHexes;
    //    transform.position = Vector3.MoveTowards(transform.position, targetPosition, step);

    //    if (Vector3.Distance(transform.position, targetPosition) <= 0.001f)
    //    {
    //        transform.position = targetPosition.Round(4);
    //        positionBeforeMoving = transform.position;
    //        tankMovementCalc.SetElapsedTimeToBackMax();
    //        currentBodyState = TankBodyStates.BackMoving;
    //        return true;
    //    }

    //    BackMovingAnimation();

    //    return false;
    //}

    //private bool BackMoving()
    //{
    //    currentBodyState = TankBodyStates.BackMoving;

    //    float step = tankMovementCalc.GetDistanceBack();
    //    Vector3 targetPosition = positionBeforeMoving - transform.forward * MapMeasures.distanceBetweenHexes;
    //    transform.position = Vector3.MoveTowards(transform.position, targetPosition, step);

    //    if (Vector3.Distance(transform.position, targetPosition) <= 0.001f)
    //    {
    //        transform.position = targetPosition.Round(4);
    //        positionBeforeMoving = transform.position;
    //        return true;
    //    }
    //    return false;
    //}

    //private bool EndBackMoving()
    //{
    //    currentBodyState = TankBodyStates.BackMoving;

    //    float step = tankMovementCalc.GetDistanceEndBack();
    //    Vector3 targetPosition = positionBeforeMoving - transform.forward * MapMeasures.distanceBetweenHexes;
    //    transform.position = Vector3.MoveTowards(transform.position, targetPosition, step);

    //    if (Vector3.Distance(transform.position, targetPosition) <= 0.001f)
    //    {
    //        transform.position = targetPosition.Round(4);
    //        positionBeforeMoving = transform.position;
    //        currentBodyState = TankBodyStates.Idle;
    //        tankMovementCalc.ResetCalculation();
    //        return true;
    //    }

    //    BackMovingAnimation();

    //    return false;
    //}

    //private void BackMovingAnimation()
    //{
    //    bool isBodyMoves = bodyActions.Count > 0;

    //    rightSideVelocity = tankMovementCalc.GetBackVelocity();

    //    trackL.Moving(rightSideVelocity, isBodyMoves);
    //    trackR.Moving(rightSideVelocity, isBodyMoves);
    //    wheelsL.Moving(rightSideVelocity);
    //    wheelsR.Moving(rightSideVelocity);
    //    body.Moving(rightSideVelocity, isBodyMoves);
    //}

    //private bool StayRotatingLeftMoving()
    //{
    //    currentBodyState = TankBodyStates.StayRotatingLeft;

    //    float angle = Vector3.Angle(rotationBeforeMoving, transform.forward);

    //    if (angle < 4.999f)
    //    {
    //        // speed up
    //        float step = tankMovementCalc.GetDegreesStartRotating();
    //        transform.Rotate(transform.up, -step);

    //        angle = Vector3.Angle(rotationBeforeMoving, transform.forward);
    //        if (angle > 4.998f)
    //        {
    //            transform.Rotate(transform.up, -(5.0f - angle));
    //            tankMovementCalc.SetElapsedTimeToRotatingMax();
    //        }
    //    }
    //    else if (angle >= 4.999f && angle < 54.999f)
    //    {
    //        // max speed
    //        float step = tankMovementCalc.GetDegreesRotating();
    //        transform.Rotate(transform.up, -step);

    //        angle = Vector3.Angle(rotationBeforeMoving, transform.forward);
    //        if (angle > 54.998f)
    //        {
    //            transform.Rotate(transform.up, -(55.0f - angle));
    //        }
    //    }
    //    else if (angle >= 54.999f && angle < 59.999f)
    //    {
    //        // speed down
    //        float step = tankMovementCalc.GetDegreesEndRotating();
    //        transform.Rotate(transform.up, -step);

    //        angle = Vector3.Angle(rotationBeforeMoving, transform.forward);
    //        if (angle > 59.998f)
    //        {
    //            transform.Rotate(transform.up, -(60.0f - angle));
    //        }
    //    }
    //    else
    //    {
    //        // finish
    //        rotationBeforeMoving = transform.forward;
    //        currentBodyState = TankBodyStates.Idle;
    //        tankMovementCalc.ResetCalculation();
    //        return true;
    //    }

    //    StayRotatingLeftMovingAnimation();

    //    return false;
    //}

    //private bool StayRotatingRightMoving()
    //{
    //    currentBodyState = TankBodyStates.StayRotatingRight;

    //    float angle = Vector3.Angle(rotationBeforeMoving, transform.forward);

    //    if (angle < 4.999f)
    //    {
    //        // speed up
    //        float step = tankMovementCalc.GetDegreesStartRotating();
    //        transform.Rotate(transform.up, step);

    //        angle = Vector3.Angle(rotationBeforeMoving, transform.forward);
    //        if (angle > 4.998f)
    //        {
    //            transform.Rotate(transform.up, (5.0f - angle));
    //            tankMovementCalc.SetElapsedTimeToRotatingMax();
    //        }
    //    }
    //    else if (angle >= 4.999f && angle < 54.999f)
    //    {
    //        // max speed
    //        float step = tankMovementCalc.GetDegreesRotating();
    //        transform.Rotate(transform.up, step);

    //        angle = Vector3.Angle(rotationBeforeMoving, transform.forward);
    //        if (angle > 54.998f)
    //        {
    //            transform.Rotate(transform.up, (55.0f - angle));
    //        }
    //    }
    //    else if (angle >= 54.999f && angle < 59.999f)
    //    {
    //        // speed down
    //        float step = tankMovementCalc.GetDegreesEndRotating();
    //        transform.Rotate(transform.up, step);

    //        angle = Vector3.Angle(rotationBeforeMoving, transform.forward);
    //        if (angle > 59.998f)
    //        {
    //            transform.Rotate(transform.up, (60.0f - angle));
    //        }
    //    }
    //    else
    //    {
    //        // finish
    //        rotationBeforeMoving = transform.forward;
    //        currentBodyState = TankBodyStates.Idle;
    //        tankMovementCalc.ResetCalculation();
    //        return true;
    //    }

    //    StayRotatingRightMovingAnimation();

    //    return false;
    //}

    //private void StayRotatingLeftMovingAnimation()
    //{
    //    rightSideVelocity = tankMovementCalc.GetRotatingVelocity();
    //    leftSideVelocity = -rightSideVelocity;

    //    trackL.Moving(leftSideVelocity, true);
    //    trackR.Moving(rightSideVelocity, true);
    //    wheelsL.Moving(leftSideVelocity);
    //    wheelsR.Moving(rightSideVelocity);
    //    body.Moving(rightSideVelocity, true);
    //}

    //private void StayRotatingRightMovingAnimation()
    //{
    //    leftSideVelocity = tankMovementCalc.GetRotatingVelocity();
    //    rightSideVelocity = -leftSideVelocity;

    //    trackL.Moving(leftSideVelocity, true);
    //    trackR.Moving(rightSideVelocity, true);
    //    wheelsL.Moving(leftSideVelocity);
    //    wheelsR.Moving(rightSideVelocity);
    //    body.Moving(leftSideVelocity, true);
    //}

    //private bool MovingRotatingLeftForwardMoving()
    //{
    //    currentBodyState = TankBodyStates.MovingRotatingLeftForward;

    //    Vector3 rotatingPoint = transform.position + -transform.right * (MapMeasures.hexOuterRaduis * 3);
    //    float stepInUnits = tankMovementCalc.GetDistanceForward();
    //    float rotatingCircleSegmentLength = 2 * Mathf.PI * (MapMeasures.hexOuterRaduis * 3) / 6;
    //    float stepInDegrees = stepInUnits / rotatingCircleSegmentLength * 60.0f;

    //    transform.RotateAround(rotatingPoint, transform.up, -stepInDegrees);

    //    rotatingPoint = transform.position + -transform.right * (MapMeasures.hexOuterRaduis * 3);
    //    float angle = Vector3.Angle(rotationBeforeMoving, transform.forward);
    //    if (angle > 59.998f)
    //    {
    //        transform.RotateAround(rotatingPoint, transform.up, -(60.0f - angle));

    //        currentBodyState = TankBodyStates.ForwardMoving;

    //        Vector3 normal = Vector3Utils.YNormal(positionBeforeMoving, rotatingPoint, true);
    //        Vector3 toLeft = ((rotatingPoint - positionBeforeMoving).normalized * (3.0f / 2.0f * MapMeasures.hexOuterRaduis));
    //        Vector3 toUp = normal * MapMeasures.hexInnerRadius * 3;

    //        Vector3 target = positionBeforeMoving + toLeft + toUp;
    //        transform.position = target.Round(4);

    //        positionBeforeMoving = transform.position;
    //        rotationBeforeMoving = transform.forward;
    //        return true;
    //    }

    //    MovingRotatingLeftForwardMovingAnimation();

    //    return false;
    //}

    //private bool MovingRotatingRightForwardMoving()
    //{
    //    return false;
    //}

    //private void MovingRotatingLeftForwardMovingAnimation()
    //{

    //}

    private void Update()
    {
        if (doActions)
        {
            if (bodyActions.Count > 0)
            {
                TankBodyActionTypes action = bodyActions[0];
                bool finishAction = false;
                switch (action)
                {
                    //case TankBodyActionTypes.StartForwardMoving:
                    //    finishAction = StartForwardMoving();
                    //    break;
                    case TankBodyActionTypes.ForwardMoving:
                        finishAction = ForwardMoving();
                        break;
                    //case TankBodyActionTypes.Stop:
                    //    finishAction = StopMoving();
                    //    break;
                        //case TankBodyActionTypes.EndForwardMoving:
                        //    finishAction = EndForwardMoving();
                        //    break;
                        //case TankBodyActionTypes.StartBackMoving:
                        //    finishAction = StartBackMoving();
                        //    break;
                        //case TankBodyActionTypes.BackMoving:
                        //    finishAction = BackMoving();
                        //    break;
                        //case TankBodyActionTypes.EndBackMoving:
                        //    finishAction = EndBackMoving();
                        //    break;
                        //case TankBodyActionTypes.StayRotatingLeft:
                        //    finishAction = StayRotatingLeftMoving();
                        //    break;
                        //case TankBodyActionTypes.StayRotatingRight:
                        //    finishAction = StayRotatingRightMoving();
                        //    break;
                        //case TankBodyActionTypes.MovingRotatingLeftForward:
                        //    finishAction = MovingRotatingLeftForwardMoving();
                        //    break;
                        //case TankBodyActionTypes.MovingRotatingRightForward:
                        //    finishAction = MovingRotatingRightForwardMoving();
                        //    break;
                }

                if (finishAction)
                {
                    bodyActions.RemoveAt(0);
                }
            }
            else
            {
                StopActions();
            }
        }

        switch (currentBodyState)
        {
            //case TankBodyStates.Idle:
            //    break;
            //case TankBodyStates.StartForwardMoving:
            //    ForwardMovingAnimation();
            //    break;
            case TankBodyStates.ForwardMoving:
                ForwardMovingAnimation();
                break;
            //case TankBodyStates.EndForwardMoving:
            //    ForwardMovingAnimation();
            //    break;
            //case TankBodyStates.StartBackMoving:
            //    BackMovingAnimation();
            //    break;
            case TankBodyStates.BackMoving:
                //BackMovingAnimation();
                break;
            //case TankBodyStates.EndBackMoving:
            //    BackMovingAnimation();
            //    break;
            //case TankBodyStates.StayRotatingLeft:
            //    StayRotatingLeftMovingAnimation();
            //    break;
            //case TankBodyStates.StayRotatingRight:
            //    StayRotatingRightMovingAnimation();
            //    break;
        }
    }

    public void SetCurrentHex(Hex hex)
    {
        currentHex = hex;
    }

    public Hex GetCurrentHex()
    {
        return currentHex;
    }

    public List<Hex> GetAvailableHexesToMove()
    {
        List<Hex> result = new List<Hex>();

        switch (currentBodyState)
        {
            case TankBodyStates.Idle:
            {
                break;
            }
            case TankBodyStates.ForwardMoving:
            {
                break;
            }
            case TankBodyStates.BackMoving:
            {
                break;
            }
        }

        return result;
    }
}
