﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TankWheelsData
{
    public int verticesCount;
    public Vector3 wheelCenter;
    public float radius;
}
