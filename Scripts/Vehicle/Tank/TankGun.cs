﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TankGun : MonoBehaviour
{
    public GameObject shell;
    private GameObject shootPoint;

    private void Start()
    {
        shootPoint = transform.Find("Shoot_Point").gameObject;
    }

    public void Shoot(Transform target)
    {
        GameObject sh = PoolObjectsManager.Instance.Get(shell);
        sh.transform.position = shootPoint.transform.position;
        sh.transform.LookAt(target);
        sh.GetComponent<Shell>().Shoot(target);
        sh.SetActive(true);
    }
}
