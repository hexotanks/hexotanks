﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum TankBodyActionTypes
{
    ForwardMoving,
    BackMoving,
    Stop,
    StayRotatingLeft,
    StayRotatingRight
}
