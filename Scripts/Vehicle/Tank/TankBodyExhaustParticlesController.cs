﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TankBodyExhaustParticlesController : MonoBehaviour
{
    private List<ParticleSystem> particles = new List<ParticleSystem>();
    private float velocity = 0.0f;
    private Vector3 direction;

    private void Start()
    {
        foreach (Transform child in transform)
        {
            particles.Add(child.GetComponent<ParticleSystem>());
        }
    }

    public void StartEffects()
    {
        foreach (ParticleSystem system in particles)
        {
            if (!system.isPlaying)
            {
                system.Play();
            }
        }
    }

    public void StopEffects()
    {
        foreach (ParticleSystem system in particles)
        {
            if (system.isPlaying)
            {
                system.Stop();
            }
        }
    }

    public void SetVelocity(float velocity)
    {
        this.velocity = velocity;
        this.direction = transform.forward;
    }

    private void LateUpdate()
    {
        if (velocity != 0.0f)
        {
            foreach (ParticleSystem system in particles)
            {
                if (system.isPlaying)
                {
                    ParticleSystem.Particle[] p = new ParticleSystem.Particle[system.particleCount + 1];
                    int l = system.GetParticles(p);

                    int i = 0;
                    while (i < l)
                    {
                        p[i].position = p[i].position + this.direction * -velocity * Time.deltaTime;
                        i++;
                    }

                    system.SetParticles(p, l);
                }
            }
        }
    }
}
