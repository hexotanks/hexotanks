﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TankWheels : MonoBehaviour
{
    public TextAsset spec;

    private List<TankWheel> wheels = new List<TankWheel>();

    private void Start()
    {
        Mesh targetMesh = GetComponent<MeshFilter>().mesh;
        List<Vector3> v = new List<Vector3>(targetMesh.vertices);

        List<string> specData = spec.ReadLines();

        int curVerticesPos = 0;
        for (int i = 0; i < specData.Count; i++)
        {
            TankWheelsData wheelsData = new SpecParser(specData[i]).GetTankWheelsData();
            int wheelVerticesCount = wheelsData.verticesCount;

            wheels.Add(new TankWheel(v.GetRange(curVerticesPos, Mathf.Min(wheelVerticesCount, v.Count - curVerticesPos)), wheelsData));

            curVerticesPos += wheelVerticesCount;
        }
    }

    public void Moving(float velocity)
    {
        if (velocity > 0.0f)
        {
            float distance = velocity * Time.deltaTime;
            List<Vector3> newVertices = new List<Vector3>();

            List<Vector3> newWheelsVertices = new List<Vector3>();
            foreach (TankWheel wheel in wheels)
            {
                newWheelsVertices = wheel.Forward(distance);
                newVertices.AddRange(newWheelsVertices);
            }

            GetComponent<MeshFilter>().mesh.vertices = newVertices.ToArray();
            GetComponent<MeshFilter>().mesh.RecalculateNormals();
        }
        else if (velocity < 0.0f)
        {
            float distance = -velocity * Time.deltaTime;
            List<Vector3> newVertices = new List<Vector3>();

            List<Vector3> newWheelsVertices = new List<Vector3>();
            foreach (TankWheel wheel in wheels)
            {
                newWheelsVertices = wheel.Back(distance);
                newVertices.AddRange(newWheelsVertices);
            }

            GetComponent<MeshFilter>().mesh.vertices = newVertices.ToArray();
            GetComponent<MeshFilter>().mesh.RecalculateNormals();
        }
        else
        {

        }
    }
}
