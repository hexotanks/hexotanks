﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TankWheel
{
    private List<Vector3> vertices;
    private Vector3 wheelCenter;
    private float circumference;

    public TankWheel(List<Vector3> vertices, TankWheelsData data)
    {
        this.vertices = vertices;

        this.wheelCenter = data.wheelCenter;
        this.circumference = 2 * Mathf.PI * data.radius;
    }

    public List<Vector3> Forward(float distance)
    {
        float targetAngle = (distance / circumference) * 360.0f;
        Quaternion rotation = Quaternion.Euler(targetAngle, 0, 0);
        for (int i = 0; i < vertices.Count; i++)
        {
            vertices[i] = rotation * (vertices[i] - wheelCenter) + wheelCenter;
        }

        return vertices;
    }

    public List<Vector3> Back(float distance)
    {
        float targetAngle = (distance / circumference) * -360.0f;
        Quaternion rotation = Quaternion.Euler(targetAngle, 0, 0);
        for (int i = 0; i < vertices.Count; i++)
        {
            vertices[i] = rotation * (vertices[i] - wheelCenter) + wheelCenter;
        }

        return vertices;
    }
}
