﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class TankTrack : MonoBehaviour
{
    public TextAsset spec;
    public GameObject curve;

    private List<Vector3> vCurve;
    private List<TankTrackLink> links = new List<TankTrackLink>();

    private TankTrackParticlesController rearForwardParticlesController;
    private TankTrackParticlesController frontForwardParticlesController;
    private TankTrackParticlesController frontBackParticlesController;
    private TankTrackParticlesController rearBackParticlesController;

    private bool areEffectsStart = false;

    private void Start()
    {
        rearForwardParticlesController = transform.Find("RearForwardParticles").GetComponent<TankTrackParticlesController>();
        frontForwardParticlesController = transform.Find("FrontForwardParticles").GetComponent<TankTrackParticlesController>();

        frontBackParticlesController = transform.Find("FrontBackParticles").GetComponent<TankTrackParticlesController>();
        rearBackParticlesController = transform.Find("RearBackParticles").GetComponent<TankTrackParticlesController>();

        Mesh mCurve = curve.GetComponent<MeshFilter>().mesh;
        vCurve = new List<Vector3>(mCurve.vertices);

        List<string> specData = spec.ReadLines();

        int linksVerticesCount = new SpecParser(specData[0]).GetTankTrackLinksVerticesCount();

        List<Vector3> v = new List<Vector3>(GetComponent<MeshFilter>().mesh.vertices);
        int dataInd = 1;
        for (int i = 0; i < v.Count; i += linksVerticesCount)
        {
            links.Add(new TankTrackLink(v.GetRange(i, Mathf.Min(linksVerticesCount, v.Count - i)), vCurve, new SpecParser(specData[dataInd]).GetTankTrackLinkData()));
            dataInd += 1;
        }
    }

    private void Forward(float velocity)
    {
        float distance = velocity * Time.deltaTime;
        List<Vector3> newVertices = new List<Vector3>();

        List<Vector3> newLinkVertices = new List<Vector3>();
        foreach (TankTrackLink link in links)
        {
            newLinkVertices = link.Forward(distance);
            newVertices.AddRange(newLinkVertices);
        }

        GetComponent<MeshFilter>().mesh.vertices = newVertices.ToArray();
        GetComponent<MeshFilter>().mesh.RecalculateNormals();
    }

    private void Back(float velocity)
    {
        float distance = -velocity * Time.deltaTime;
        List<Vector3> newVertices = new List<Vector3>();

        List<Vector3> newLinkVertices = new List<Vector3>();
        foreach (TankTrackLink link in links)
        {
            newLinkVertices = link.Back(distance);
            newVertices.AddRange(newLinkVertices);
        }

        GetComponent<MeshFilter>().mesh.vertices = newVertices.ToArray();
        GetComponent<MeshFilter>().mesh.RecalculateNormals();
    }

    public void Moving(float velocity, bool isBodyMoving)
    {
        if (velocity > 0.0f)
        {
            Forward(velocity);
        }
        else if (velocity < 0.0f)
        {
            Back(velocity);
        }
        else
        {

        }

        if (velocity > 1.5f && !areEffectsStart)
        {
            rearForwardParticlesController.StartEffects();
            frontForwardParticlesController.StartEffects();
            areEffectsStart = true;
        }
        else if (velocity < -1.0f && !areEffectsStart)
        {
            rearBackParticlesController.StartEffects();
            frontBackParticlesController.StartEffects();
            areEffectsStart = true;
        }
        else if ((velocity <= 1.5f && velocity >= -1.0f) && areEffectsStart)
        {
            rearForwardParticlesController.StopEffects();
            frontForwardParticlesController.StopEffects();
            frontBackParticlesController.StopEffects();
            rearBackParticlesController.StopEffects();
            areEffectsStart = false;
        }

        if (!isBodyMoving)
        {
            rearForwardParticlesController.SetVelocity(velocity);
            frontForwardParticlesController.SetVelocity(velocity);
            frontBackParticlesController.SetVelocity(velocity);
            rearBackParticlesController.SetVelocity(velocity);
        }
        else
        {
            rearForwardParticlesController.SetVelocity(0.0f);
            frontForwardParticlesController.SetVelocity(0.0f);
            frontBackParticlesController.SetVelocity(0.0f);
            rearBackParticlesController.SetVelocity(0.0f);
        }
    }

    public void Rotate(float velocity)
    {

    }    
}
