﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TankBody : MonoBehaviour
{
    private TankBodyExhaustParticlesController forwardExhaustParticlesController;
    private bool areEffectsStart = false;

    private void Start()
    {
        forwardExhaustParticlesController = transform.Find("ForwardExhaustParticles").GetComponent<TankBodyExhaustParticlesController>();
    }

    public void Moving(float velocity, bool isBodyMoving)
    {
        if ((velocity > 1.0f || velocity < -0.5f) && !areEffectsStart)
        {
            forwardExhaustParticlesController.StartEffects();
            areEffectsStart = true;
        }
        else if ((velocity <= 1.0f && velocity >= -0.5f) && areEffectsStart)
        {
            forwardExhaustParticlesController.StopEffects();
            areEffectsStart = false;
        }

        if (!isBodyMoving)
        {
            forwardExhaustParticlesController.SetVelocity(velocity);
        }
        else
        {
            forwardExhaustParticlesController.SetVelocity(0.0f);
        }
    }
}
