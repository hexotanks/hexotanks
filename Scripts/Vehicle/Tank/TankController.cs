﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TankController : MonoBehaviour
{
    private Tank tank;

    private bool isTankDoAction = false;

    // for test
    private bool isRepeat = false;
    private int maxRepeat = 204;
    private int currentRepeat = 0;

    private void Start()
    {
        tank = gameObject.GetComponent<Tank>();

        tank.OnFinishDoAllActions += TankFinishDoAllActions;
    }

    private void TankFinishDoAllActions()
    {
        isTankDoAction = false;
    }

    private void TankStartDoActions()
    {
        isTankDoAction = true;
        tank.StartActions();
    }

    private void Update()
    {
        if (!isTankDoAction)
        {
            // for test
            //if (isRepeat && currentRepeat < maxRepeat)
            //{
            //    tank.AddBodyAction(TankBodyActionTypes.MovingRotatingLeftForward);
            //    TankStartDoActions();
            //    currentRepeat += 1;
            //}

            //if (Input.GetKey(KeyCode.W))
            //{
            //    tank.AddBodyAction(TankBodyActionTypes.ForwardMoving);
            //    TankStartDoActions();
            //}
            //if (Input.GetKey(KeyCode.Space))
            //{
            //    tank.AddBodyAction(TankBodyActionTypes.Stop);
            //    TankStartDoActions();
            //}

            //if (Input.GetKey(KeyCode.C))
            //{
            //    tank.AddBodyAction(TankBodyActionTypes.ForwardMoving);
            //    TankStartDoActions();
            //}
            //if (Input.GetKey(KeyCode.V))
            //{
            //    tank.AddBodyAction(TankBodyActionTypes.BackMoving);
            //    TankStartDoActions();
            //}
            //else if (Input.GetKey(KeyCode.S))
            //{
            //    tank.AddBodyAction(TankBodyActionTypes.StartBackMoving);
            //    TankStartDoActions();
            //}
            //else if (Input.GetKey(KeyCode.Z))
            //{
            //    tank.AddBodyAction(TankBodyActionTypes.EndForwardMoving);
            //    TankStartDoActions();
            //}
            //else if (Input.GetKey(KeyCode.X))
            //{
            //    tank.AddBodyAction(TankBodyActionTypes.EndBackMoving);
            //    TankStartDoActions();
            //}
            //else if (Input.GetKey(KeyCode.A))
            //{
            //    tank.AddBodyAction(TankBodyActionTypes.StayRotatingLeft);
            //    TankStartDoActions();
            //}
            //else if (Input.GetKey(KeyCode.D))
            //{
            //    tank.AddBodyAction(TankBodyActionTypes.StayRotatingRight);
            //    TankStartDoActions();
            //}
            //else if (Input.GetKey(KeyCode.Q))
            //{
            //    tank.AddBodyAction(TankBodyActionTypes.MovingRotatingLeftForward);
            //    TankStartDoActions();
            //}
            //else if (Input.GetKey(KeyCode.E))
            //{
            //    tank.AddBodyAction(TankBodyActionTypes.MovingRotatingRightForward);
            //    TankStartDoActions();
            //}
        }


        //if (Input.GetKeyDown(KeyCode.Q))
        //{
        //    turret.Left();
        //}
        //if (Input.GetKeyDown(KeyCode.E))
        //{
        //    turret.Right();
        //}
        //if (Input.GetKeyDown(KeyCode.F))
        //{
        //    if (turret.CanShoot(target))
        //    {
        //        turret.Aim(target);
        //    }
        //    else
        //    {
        //        Debug.Log("Can't aim");
        //    }
        //}
        //if (Input.GetKeyDown(KeyCode.G))
        //{
        //    turret.CancelAim();
        //}
        //if (Input.GetKeyDown(KeyCode.Space))
        //{
        //    if (turret.CanShoot(target))
        //    {
        //        turret.Shoot(target);
        //    }
        //    else
        //    {
        //        Debug.Log("Can't shoot");
        //    }
        //}
    }
}
