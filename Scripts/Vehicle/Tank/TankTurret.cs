﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TankTurret : MonoBehaviour
{
    private TankGun gun;
    private TankMantlet mantlet;
    private bool isRotateLeft = false;
    private bool isRotateRight = false;

    private float horizontalSpeed;
    private float maxAngleLeft = 180.0f;
    private float maxAngleRight = -180.0f;
    private float rotationAngle = 60.0f;

    private float remainRotateAngle = 60.0f;
    private float angleBeforeAim = 0.0f;

    void Start()
    {
        gun = transform.Find("Mantlet/Gun").GetComponent<TankGun>();
        mantlet = transform.Find("Mantlet").GetComponent<TankMantlet>();
    }

    public void SetSpeed(float horizontalSpeed, float verticalSpeed)
    {
        this.horizontalSpeed = horizontalSpeed;
        mantlet.SetVerticalSpeed(verticalSpeed);
    }

    public void Left()
    {
        isRotateLeft = true;
    }

    public void Right()
    {
        isRotateRight = true;
    }

    private void RotateLeft()
    {
        float targetAngle = Time.deltaTime * horizontalSpeed;
        if (remainRotateAngle > targetAngle)
        {
            transform.Rotate(0.0f, -targetAngle, 0.0f);
            remainRotateAngle -= targetAngle;
        }
        else
        {
            transform.Rotate(0.0f, -remainRotateAngle, 0.0f);
            remainRotateAngle = rotationAngle;
            isRotateLeft = false;
        }
    }

    private void RotateRight()
    {
        float targetAngle = Time.deltaTime * horizontalSpeed;
        if (remainRotateAngle > targetAngle)
        {
            transform.Rotate(0.0f, targetAngle, 0.0f);
            remainRotateAngle -= targetAngle;
        }
        else
        {
            transform.Rotate(0.0f, remainRotateAngle, 0.0f);
            remainRotateAngle = rotationAngle;
            isRotateRight = false;
        }
    }

    public float GetHorizontalSignedAngle(Transform target)
    {
        Vector3 targetPos = target.position - transform.position;
        targetPos.y = 0.0f;
        Vector3 pos = transform.forward;
        pos.y = 0.0f;
        float angle = Vector3.SignedAngle(targetPos, pos, transform.up);
        return angle;
    }

    public bool CanShoot(Transform target)
    {
        float angle = GetHorizontalSignedAngle(target);
        if (angle > maxAngleLeft || angle < maxAngleRight)
        {
            return false;
        }
        return true;
    }

    public void Shoot(Transform target)
    {
        gun.Shoot(target);
    }

    public void Aim(Transform target)
    {
        float angle = GetHorizontalSignedAngle(target);
        angleBeforeAim += angle;
        if (angle > 0.0f)
        {
            remainRotateAngle = angle;
            isRotateLeft = true;
        }
        else if (angle < 0.0f)
        {
            remainRotateAngle = -angle;
            isRotateRight = true;
        }
        mantlet.Aim(target, angle);
    }

    public void CancelAim()
    {
        if (angleBeforeAim < 0.0f)
        {
            remainRotateAngle = -angleBeforeAim;
            isRotateLeft = true;
        }
        else if (angleBeforeAim > 0.0f)
        {
            remainRotateAngle = angleBeforeAim;
            isRotateRight = true;
        }
        mantlet.CancelAim();
        angleBeforeAim = 0.0f;
    }    

    private void Update()
    {
        if (isRotateLeft)
        {
            RotateLeft();
        }
        if (isRotateRight)
        {
            RotateRight();
        }
    }
}
