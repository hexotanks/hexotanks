﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TankMovementCalculation
{
    //private float hexSegmentLength;

    private float forwardMaxSpeed;
    private float forwardAccelerationTime;
    private float forwardCoef;

    //private float backMaxSpeed;
    //private float backAccelerationTime;
    //private float backCoef;

    //private float rotatingMaxSpeedInDegress;
    //private float rotatingAccelerationTime;
    //private float rotatingCoef;

    //private float prevShift = 0.0f;

    //private float elapsedTime = 0.0f;

    //private Vector2 currentSpeed = Vector2.zero;

    private float currentSpeed = 0.0f;
    private float currentRotateAngle = 0.0f;

    private float curvatureLimit = 0.2f;
    private float rotateAngleLimit = 45.0f;

    private int lastRecalculation = -1;

    public TankMovementCalculation(float forwardMaxSpeed)
    {
        this.forwardMaxSpeed = forwardMaxSpeed;
        forwardCoef = forwardMaxSpeed * forwardMaxSpeed / (4 * MapMeasures.distanceBetweenHexes);
        this.forwardAccelerationTime = Mathf.Sqrt(MapMeasures.distanceBetweenHexes / forwardCoef);

        //this.backMaxSpeed = forwardMaxSpeed / 2.0f;
        //backCoef = backMaxSpeed * backMaxSpeed / (4 * MapMeasures.distanceBetweenHexes);
        //this.backAccelerationTime = Mathf.Sqrt(MapMeasures.distanceBetweenHexes / backCoef);

        //this.rotatingMaxSpeedInDegress = forwardMaxSpeed * 3.0f;
        //// 5 degrees
        //rotatingCoef = rotatingMaxSpeedInDegress * rotatingMaxSpeedInDegress / (4 * 5.0f);
        //this.rotatingAccelerationTime = Mathf.Sqrt(5.0f / rotatingCoef);
    }

    public void Forward(float curvature)
    {
        float delta = Time.deltaTime / forwardAccelerationTime;

        float curvatureSign = Mathf.Sign(curvature);
        curvature = Mathf.Abs(curvature);
        curvature = Mathf.Clamp(curvature, 0.0f, curvatureLimit);
        float curvatureCoef = (1 - (curvature / curvatureLimit) * 0.5f);

        currentSpeed += forwardMaxSpeed * delta * curvatureCoef;
        currentSpeed = Mathf.Clamp(currentSpeed, 0.0f, forwardMaxSpeed * curvatureCoef);
        currentRotateAngle = rotateAngleLimit / curvatureLimit * curvature * curvatureSign;
    }

    public float GetVelocity()
    {
        return currentSpeed;
        //if (CanRecalculate())
        //{
        //    Debug.Log("Speed: " + currentSpeed);
        //    return currentSpeed;
        //if (currentSpeed.y > 0)
        //{
        //    // forward

        //    //float speed = currentSpeed.y * forwardMaxSpeed;
        //    Debug.Log("Speed: " + currentSpeed);
        //    return currentSpeed;
        //}
        //else if (currentSpeed.y < 0)
        //{
        //    // back
        //}
        //else
        //{
        //    if (currentSpeed.x < 0)
        //    {
        //        // rotate left
        //    }
        //    else if (currentSpeed.x > 0)
        //    {
        //        // rotate right
        //    }
        //    else
        //    {
        //        // stop
        //    }
        //}
        //}


        //return 0.0f;
    }

    public float GetLeftTrackVelocity()
    {
        if (currentRotateAngle < 0.0f && currentSpeed > 0.0f)
        {
            float rotateCoef = (1 - (-currentRotateAngle / rotateAngleLimit) * 0.8f);
            return currentSpeed * rotateCoef;
        }
        //else if (currentRotateAngle > 0.0f && currentSpeed < 0.0f)
        //{
        //    float rotateCoef = (1 - (currentRotateAngle / rotateAngleLimit) * 0.8f);
        //    return currentSpeed * rotateCoef;
        //}
        return currentSpeed;
    }

    public float GetRightTrackVelocity()
    {
        if (currentRotateAngle > 0.0f && currentSpeed > 0.0f)
        {
            float rotateCoef = (1 - (currentRotateAngle / rotateAngleLimit) * 0.8f);
            return currentSpeed * rotateCoef;
        }
        //else if (currentRotateAngle < 0.0f && currentSpeed > 0.0f)
        //{
        //    float rotateCoef = (1 - (-currentRotateAngle / rotateAngleLimit) * 0.8f);
        //    return currentSpeed * rotateCoef;
        //}
        return currentSpeed;
    }

    //public float GetDistanceStartForward()
    //{
    //    elapsedTime += Time.deltaTime;
    //    elapsedTime = Mathf.Clamp(elapsedTime, 0.0f, forwardAccelerationTime);
    //    float newShiftPosition = forwardCoef * elapsedTime * elapsedTime;
    //    float diff = newShiftPosition - prevShift;
    //    prevShift = newShiftPosition;
    //    return diff;
    //}

    //public float GetDistanceForward()
    //{
    //    return 2 * forwardCoef * forwardAccelerationTime * Time.deltaTime;
    //}

    //public float GetDistanceEndForward()
    //{
    //    elapsedTime -= Time.deltaTime;
    //    elapsedTime = Mathf.Clamp(elapsedTime, 0.0f, forwardAccelerationTime);
    //    float newShiftPosition = forwardCoef * elapsedTime * elapsedTime;
    //    float diff = prevShift - newShiftPosition;
    //    prevShift = newShiftPosition;
    //    return diff;
    //}

    //public float GetForwardVelocity()
    //{
    //    return 2 * forwardCoef * elapsedTime;
    //}

    //public float GetDistanceStartBack()
    //{
    //    elapsedTime += Time.deltaTime;
    //    elapsedTime = Mathf.Clamp(elapsedTime, 0.0f, backAccelerationTime);
    //    float newShiftPosition = backCoef * elapsedTime * elapsedTime;
    //    float diff = newShiftPosition - prevShift;
    //    prevShift = newShiftPosition;
    //    return diff;
    //}

    //public float GetDistanceBack()
    //{
    //    return 2 * backCoef * backAccelerationTime * Time.deltaTime;
    //}

    //public float GetDistanceEndBack()
    //{
    //    elapsedTime -= Time.deltaTime;
    //    elapsedTime = Mathf.Clamp(elapsedTime, 0.0f, backAccelerationTime);
    //    float newShiftPosition = backCoef * elapsedTime * elapsedTime;
    //    float diff = prevShift - newShiftPosition;
    //    prevShift = newShiftPosition;
    //    return diff;
    //}

    //public float GetBackVelocity()
    //{
    //    return -2 * backCoef * elapsedTime;
    //}

    //public float GetDegreesStartRotating()
    //{
    //    elapsedTime += Time.deltaTime;
    //    elapsedTime = Mathf.Clamp(elapsedTime, 0.0f, rotatingAccelerationTime);
    //    float newShiftRotating = rotatingCoef * elapsedTime * elapsedTime;
    //    float diff = newShiftRotating - prevShift;
    //    prevShift = newShiftRotating;
    //    return diff;
    //}

    //public float GetDegreesRotating()
    //{
    //    return 2 * rotatingCoef * rotatingAccelerationTime * Time.deltaTime;
    //}

    //public float GetDegreesEndRotating()
    //{
    //    elapsedTime -= Time.deltaTime;
    //    elapsedTime = Mathf.Clamp(elapsedTime, 0.0f, rotatingAccelerationTime);
    //    float newShiftPosition = rotatingCoef * elapsedTime * elapsedTime;
    //    float diff = prevShift - newShiftPosition;
    //    prevShift = newShiftPosition;
    //    return diff;
    //}

    //public float GetRotatingVelocity()
    //{
    //    float velocityInDegrees = 2 * rotatingCoef * elapsedTime;
    //    return (velocityInDegrees / 12) * hexSegmentLength / 2;
    //}

    //public void SetElapsedTimeToForwardMax()
    //{
    //    elapsedTime = forwardAccelerationTime;
    //}

    //public void SetElapsedTimeToBackMax()
    //{
    //    elapsedTime = backAccelerationTime;
    //}

    //public void SetElapsedTimeToRotatingMax()
    //{
    //    elapsedTime = rotatingAccelerationTime;
    //}

    //public void ResetCalculation()
    //{
    //    elapsedTime = 0.0f;
    //    prevShift = 0.0f;
    //}

    private bool CanRecalculate()
    {
        if (lastRecalculation == Time.frameCount)
        {
            return false;
        }
        else
        {
            lastRecalculation = Time.frameCount;
            return true;
        }
    }
}
