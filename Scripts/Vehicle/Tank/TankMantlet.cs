﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TankMantlet : MonoBehaviour
{
    private float verticalSpeed;
    private bool isRotateUp = false;
    private bool isRotateDown = false;

    private float remainRotateAngle;
    private float angleBeforeAim = 0.0f;

    public void SetVerticalSpeed(float verticalSpeed)
    {
        this.verticalSpeed = verticalSpeed;
    }

    private void RotateUp()
    {
        float targetAngle = Time.deltaTime * verticalSpeed;
        if (remainRotateAngle > targetAngle)
        {
            transform.Rotate(-targetAngle, 0.0f, 0.0f);
            remainRotateAngle -= targetAngle;
        }
        else
        {
            transform.Rotate(-remainRotateAngle, 0.0f, 0.0f);
            remainRotateAngle = 0.0f;
            isRotateUp = false;
        }
    }

    private void RotateDown()
    {
        float targetAngle = Time.deltaTime * verticalSpeed;
        if (remainRotateAngle > targetAngle)
        {
            transform.Rotate(targetAngle, 0.0f, 0.0f);
            remainRotateAngle -= targetAngle;
        }
        else
        {
            transform.Rotate(remainRotateAngle, 0.0f, 0.0f);
            remainRotateAngle = 0.0f;
            isRotateDown = false;
        }
    }

    public float GetVerticalSignedAngle(Transform target, float horizontalAngle)
    {
        Vector3 positionAfterHorizontalRotation = Quaternion.AngleAxis(-horizontalAngle, transform.parent.transform.up) * transform.position;
        Vector3 vectorToTarget = target.position - positionAfterHorizontalRotation;
        Vector3 forwardAfterHorizontalRotation = Quaternion.AngleAxis(-horizontalAngle, transform.parent.transform.up) * transform.forward;
        Vector3 right = Vector3.Cross(forwardAfterHorizontalRotation, transform.parent.transform.up).normalized;
        float angle = Vector3.SignedAngle(vectorToTarget, forwardAfterHorizontalRotation, right);
        return angle;
    }

    public void Aim(Transform target, float horizontalAngle)
    {
        float angle = GetVerticalSignedAngle(target, horizontalAngle);
        angleBeforeAim += angle;
        if (angle < 0.0f)
        {
            remainRotateAngle = -angle;
            isRotateUp = true;
        }
        else if (angle > 0.0f)
        {
            remainRotateAngle = angle;
            isRotateDown = true;
        }
    }

    public void CancelAim()
    {
        if (angleBeforeAim > 0.0f)
        {
            remainRotateAngle = angleBeforeAim;
            isRotateUp = true;
        }
        else if (angleBeforeAim < 0.0f)
        {
            remainRotateAngle = -angleBeforeAim;
            isRotateDown = true;
        }
        angleBeforeAim = 0.0f;
    }

    private void Update()
    {
        if (isRotateUp)
        {
            RotateUp();
        }
        if (isRotateDown)
        {
            RotateDown();
        }
    }
}
