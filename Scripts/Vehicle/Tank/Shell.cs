﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shell : MonoBehaviour
{
    private float speed = 1.0f;
    private Transform target;

    public void Shoot(Transform target)
    {
        this.target = target;
    }

    private void Fly()
    {
        float currentSpeed = Time.deltaTime * speed;
        float distance = Vector3.Distance(target.position, transform.position);
        Vector3 direction = (target.transform.position - transform.position).normalized;
        if (distance - currentSpeed <= 0.0f)
        {
            transform.Translate(direction * distance, Space.World);
            Hit();
        }
        else
        {
            transform.Translate(direction * currentSpeed, Space.World);
        }
    }

    private void Hit()
    {

    }

    private void Update()
    {
        Fly();
    }
}
