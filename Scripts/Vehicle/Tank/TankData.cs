﻿using UnityEngine;

public class TankData
{
    public Vector3 shootPoint;
    public string caliber;
    public string nation;
}
