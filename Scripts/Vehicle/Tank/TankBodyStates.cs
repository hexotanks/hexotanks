﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum TankBodyStates
{
    Idle,
    ForwardMoving,
    BackMoving,
    StayRotatingLeft,
    StayRotatingRight
}
