﻿using System.Collections;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;

public class TankTrackLink
{
    private List<Vector3> vertices;
    private Vector3 linkCenter;
    private List<Vector3> vCurve;

    private int v1;
    private int v2;
    private float angleX;
    public TankTrackLink(List<Vector3> vertices, List<Vector3> vCurve, TankTrackLinkData data)
    {
        this.vertices = vertices;
        this.vCurve = vCurve;

        this.linkCenter = data.linkCenter;
        this.v1 = data.v1;
        this.v2 = data.v2;
        this.angleX = data.angleX;
    }

    public List<Vector3> Forward(float distance)
    {
        float currentDistance = (linkCenter - vCurve[v1]).magnitude;
        if ((vCurve[v2] - vCurve[v1]).magnitude >= currentDistance + distance)
        {
            Vector3 newPos = ((currentDistance + distance) / (vCurve[v2] - vCurve[v1]).magnitude) * (vCurve[v2] - vCurve[v1]) + vCurve[v1];
            Vector3 shift = newPos - linkCenter;

            for(int i = 0; i < vertices.Count; i++)
            {
                vertices[i] = vertices[i] + shift;
            }

            linkCenter = newPos;

            return vertices;
        }
        else
        {
            currentDistance = -1.0f * (distance - (currentDistance + distance - (vCurve[v2] - vCurve[v1]).magnitude));
            v1 = v1 + 1;
            v2 = v2 + 1;
            if (v2 == vCurve.Count)
            {
                v2 = 0;
            }
            if (v1 == vCurve.Count)
            {
                v1 = 0;
            }

            Vector3 newPos = ((currentDistance + distance) / (vCurve[v2] - vCurve[v1]).magnitude) * (vCurve[v2] - vCurve[v1]) + vCurve[v1];

            Vector3 targetDir = vCurve[v2] - vCurve[v1];
            float newAngle = Vector3.SignedAngle(targetDir, Vector3.forward, Vector3.forward);
            if (vCurve[v1].y <= vCurve[v2].y)
            {
                newAngle = 0 + newAngle;
            }
            else
            {
                newAngle = 0 - newAngle;
            }

            float targetAngle = angleX - newAngle;

            Vector3 shift = newPos - linkCenter;
            Quaternion rotation = Quaternion.Euler(targetAngle, 0, 0);
            for (int i = 0; i < vertices.Count; i++)
            {
                vertices[i] = vertices[i] + shift;
                vertices[i] = rotation * (vertices[i] - newPos) + newPos;
            }

            angleX = newAngle;
            linkCenter = newPos;

            return vertices;
        }
    }

    public List<Vector3> Back(float distance)
    {
        float currentDistance = (vCurve[v1] - linkCenter).magnitude;
        if (currentDistance - distance >= 0.0f)
        {
            Vector3 newPos = (((vCurve[v2] - vCurve[v1]).magnitude - (currentDistance - distance)) / (vCurve[v2] - vCurve[v1]).magnitude) * (vCurve[v1] - vCurve[v2]) + vCurve[v2];
            Vector3 shift = newPos - linkCenter;

            for (int i = 0; i < vertices.Count; i++)
            {
                vertices[i] = vertices[i] + shift;
            }

            linkCenter = newPos;

            return vertices;
        }
        else
        {
            v1 = v1 - 1;
            v2 = v2 - 1;
            if (v2 == -1)
            {
                v2 = vCurve.Count - 1;
            }
            if (v1 == -1)
            {
                v1 = vCurve.Count - 1;
            }

            currentDistance = (vCurve[v2] - vCurve[v1]).magnitude + currentDistance;

            Vector3 newPos = (((vCurve[v2] - vCurve[v1]).magnitude - (currentDistance - distance)) / (vCurve[v2] - vCurve[v1]).magnitude) * (vCurve[v1] - vCurve[v2]) + vCurve[v2];

            Vector3 targetDir = vCurve[v2] - vCurve[v1];
            float newAngle = Vector3.SignedAngle(targetDir, Vector3.forward, Vector3.forward);
            if (vCurve[v1].y <= vCurve[v2].y)
            {
                newAngle = 0 + newAngle;
            }
            else
            {
                newAngle = 0 - newAngle;
            }

            float targetAngle = newAngle - angleX;
            Vector3 shift = newPos - linkCenter;
            Quaternion rotation = Quaternion.Euler(-targetAngle, 0, 0);
            for (int i = 0; i < vertices.Count; i++)
            {
                vertices[i] = vertices[i] + shift;
                vertices[i] = rotation * (vertices[i] - newPos) + newPos;
            }

            angleX = newAngle;
            linkCenter = newPos;

            return vertices;
        }
    }
}
