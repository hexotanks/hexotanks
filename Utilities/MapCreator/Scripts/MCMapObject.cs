﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace Utilities.MapCreator
{
    [Serializable]
    public class MCMapObject
    {
        public string name;
        public List<MCHexObject> hexes;
    }
}
