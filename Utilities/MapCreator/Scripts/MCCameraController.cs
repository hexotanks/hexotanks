﻿using UnityEngine;
using UnityEditor;
using System.IO;
using System.Collections.Generic;
using System.Linq;

namespace Utilities.MapCreator
{
    // MC - MapCreator
    public class MCCameraController : MonoBehaviour
    {
        private Camera camera;
        private MCGUI gui;

        private void Start()
        {
            camera = GetComponent<Camera>();
            gui = GetComponent<MCGUI>();
        }

        // Update is called once per frame
        private void Update()
        {
            camera.transform.Translate(Input.GetAxis("Horizontal"), -Input.GetAxis("MouseScrollWheel") * 30.0f, Input.GetAxis("Vertical"), Space.World);

            if (Input.GetMouseButtonDown(0))
            {
                RaycastHit hitInfo = new RaycastHit();
                bool hit = Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hitInfo);
                if (hit)
                {
                    GameObject hitGO = hitInfo.transform.gameObject;
                    GameObject prefab = gui.GetSelectedHex();

                    if (prefab.name.Contains("SpawnPoint"))
                    {
                        if (!hitGO.GetComponent<MCHex>().hasSpawnPoint)
                        {
                            GameObject spawnPoint = Instantiate(prefab, hitGO.transform);
                            spawnPoint.transform.position = hitGO.transform.position;
                            hitGO.GetComponent<MCHex>().hasSpawnPoint = true;
                        }
                    }
                    else if (prefab.name.Contains("Hex"))
                    {
                        GameObject hex = Instantiate(prefab, hitGO.transform.parent);
                        hex.transform.position = hitGO.transform.position;
                        hex.name = hitGO.name;
                        hex.GetComponent<MCHex>().x = hitGO.GetComponent<MCHex>().x;
                        hex.GetComponent<MCHex>().y = hitGO.GetComponent<MCHex>().y;

                        if (hitGO.GetComponent<MCHex>().hasSpawnPoint)
                        {
                            hex.GetComponent<MCHex>().hasSpawnPoint = true;
                            foreach (Transform child in hitGO.transform)
                            {
                                child.SetParent(hex.transform);
                            }
                        }

                        Destroy(hitGO);
                    }
                }
            }
        }
    }
}
