﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace Utilities.MapCreator
{
    [Serializable]
    public class MCHexObject
    {
        [Serializable]
        public enum Types
        {
            Template,
            Ground,
            Water
        }

        public Types type;
        public int x;
        public int y;

        public bool hasSpawnPoint;
    }
}
