﻿using UnityEngine;
using UnityEditor;
using System.IO;
using System.Collections.Generic;
using System.Linq;

namespace Utilities.MapCreator
{
    public class MCGUI : MonoBehaviour
    {
        private int selectedHexIndex = 0;
        private string[] hexNames = new string[] { "Ground", "Water", "----", "SpawnPoint" };
        private Dictionary<int, GameObject> hexesDict = new Dictionary<int, GameObject>();

        private void Start()
        {
            hexesDict.Add(0, GetHexPrefab("Template"));
            hexesDict.Add(1, GetHexPrefab("Ground"));
            hexesDict.Add(2, GetHexPrefab("Water"));
        }

        private void OnGUI()
        {
            selectedHexIndex = GUI.SelectionGrid(new Rect(Screen.width - 150, 50, 100, hexNames.Length * 25), selectedHexIndex, hexNames, 1);

            if (GUI.Button(new Rect(Screen.width - 150, Screen.height - 100, 100, 50), "Update Map"))
            {
                UpdateMap();
            }
        }

        private GameObject GetHexPrefab(string name)
        {
            string hexPrefabFolderPath = "Assets/Utilities/MapCreator/Prefabs";
            string[] hexGUIDs = AssetDatabase.FindAssets("MCHex" + name, new[] { hexPrefabFolderPath });
            string hexPath = AssetDatabase.GUIDToAssetPath(hexGUIDs[0]);
            return (GameObject)AssetDatabase.LoadAssetAtPath(hexPath, typeof(GameObject));
        }

        private GameObject GetSpawnPointPrefab()
        {
            string prefabFolderPath = "Assets/Utilities/MapCreator/Prefabs";
            string[] GUIDs = AssetDatabase.FindAssets("MCSpawnPoint", new[] { prefabFolderPath });
            string path = AssetDatabase.GUIDToAssetPath(GUIDs[0]);
            return (GameObject)AssetDatabase.LoadAssetAtPath(path, typeof(GameObject));
        }

        private void UpdateMap()
        {
            GameObject map = GameObject.Find("MCMap");
            string hexPrefabFolderPath = "Assets/Utilities/MapCreator/Prefabs";
            PrefabUtility.SaveAsPrefabAssetAndConnect(map, hexPrefabFolderPath + "/MCMap.prefab", InteractionMode.AutomatedAction);
        }

        public GameObject GetSelectedHex()
        {
            if (selectedHexIndex == hexNames.Length - 1)
            {
                return GetSpawnPointPrefab();
            }
            return hexesDict[selectedHexIndex + 1];
        }
    }
}
