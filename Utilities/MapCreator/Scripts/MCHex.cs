﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Utilities.MapCreator
{
    public class MCHex : MonoBehaviour
    {
        public enum Types
        {
            Template,
            Ground,
            Water
        }

        public Types type;
        public int x;
        public int y;

        public bool hasSpawnPoint = false;
        public bool canShoot = false;
        public bool canMove = false;
    }
}
