﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace Utilities.MapCreator
{
    public class MCMap : MonoBehaviour
    {
        public MCMapObject GetMap(string mapName)
        {
            List<MCHex> hexes = new List<MCHex>();
            foreach (Transform child in transform)
            {
                hexes.Add(child.GetComponent<MCHex>());
            }

            MCMapObject mapObj = new MCMapObject();
            mapObj.name = mapName.CapitalizeFirstLetter();
            mapObj.hexes = new List<MCHexObject>();

            foreach (var hex in hexes)
            {
                MCHexObject h = new MCHexObject();
                h.type = (MCHexObject.Types)Enum.Parse(typeof(MCHexObject.Types), hex.type.ToString());
                h.x = hex.x;
                h.y = hex.y;
                h.hasSpawnPoint = hex.hasSpawnPoint;
                mapObj.hexes.Add(h);
            }

            return mapObj;
        }
    }
}
