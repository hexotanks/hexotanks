﻿using UnityEngine;
using UnityEditor;
using System.IO;
using System.Collections.Generic;

namespace Utilities.MapCreator
{
    public class MapCreator : EditorWindow
    {
        private string heightEdit = "5";
        private string widthEdit = "5";
        private string pathToFileEdit = "";
        private float hexHeight;
        private float hexWidth;

        private string mcHexPrefabFolderPath = "Assets/Utilities/MapCreator/Prefabs";

        private Dictionary<string, GameObject> mcHexesDict = new Dictionary<string, GameObject>();
        private Dictionary<string, GameObject> hexesDict = new Dictionary<string, GameObject>();

        [MenuItem("Create/Map")]
        static void Init()
        {
            MapCreator window = (MapCreator)EditorWindow.GetWindow(typeof(MapCreator));
            window.titleContent.text = "Map Creator";
            window.Show();
        }

        private void OnEnable()
        {
            mcHexesDict.Add("Template", GetMCHexPrefab("Template"));
            mcHexesDict.Add("Ground", GetMCHexPrefab("Ground"));
            mcHexesDict.Add("Water", GetMCHexPrefab("Water"));

            hexesDict.Add("Ground", GetHexPrefab("Ground"));
            hexesDict.Add("Water", GetHexPrefab("Water"));

            hexWidth = MapMeasures.hexOuterRaduis * 2;
            hexHeight = MapMeasures.hexInnerRadius * 2;
        }

        private void OnGUI()
        {
            GUILayout.Label("Map Options", EditorStyles.boldLabel);

            GUI.Label(new Rect(5, 20, 50, 20), "Height");
            GUI.Label(new Rect(95, 20, 50, 20), "Width");

            heightEdit = GUI.TextField(new Rect(60, 20, 30, 20), heightEdit);
            widthEdit = GUI.TextField(new Rect(150, 20, 30, 20), widthEdit);

            if (GUI.Button(new Rect(position.width - 123, 45, 120, 25), "Generate Template"))
            {
                GameObject map = GameObject.Find("MCMap");
                if (map != null)
                {
                    DestroyImmediate(map);
                }

                map = new GameObject("MCMap");
                map.AddComponent<MCMap>();

                //var tempList = map.transform.Cast<Transform>().ToList();
                //foreach (var child in tempList)
                //{
                //    DestroyImmediate(child.gameObject);
                //}

                string[] hexGUIDs = AssetDatabase.FindAssets("MCHexTemplate", new[] { mcHexPrefabFolderPath });
                string hexPath = AssetDatabase.GUIDToAssetPath(hexGUIDs[0]);
                GameObject hexPrefab = (GameObject)AssetDatabase.LoadAssetAtPath(hexPath, typeof(GameObject));

                int height = int.Parse(heightEdit);
                int width = int.Parse(widthEdit);

                for (var i = 0; i < height; i++)
                {
                    for (var j = 0; j < width; j++)
                    {
                        Vector3 hexPosition = new Vector3(hexWidth * 3 / 4 * j, 0.0f, hexHeight / 2 * (j % 2) + hexHeight * i);
                        GameObject hex = PrefabUtility.InstantiatePrefab(hexPrefab, map.transform) as GameObject;
                        hex.transform.position = hexPosition;
                        hex.name = "Hex" + i + j;
                        hex.GetComponent<MCHex>().x = i;
                        hex.GetComponent<MCHex>().y = j;
                    }
                }

                PrefabUtility.SaveAsPrefabAssetAndConnect(map, mcHexPrefabFolderPath + "/MCMap.prefab", InteractionMode.AutomatedAction);
            }

            GUI.Label(new Rect(5, 75, 150, 20), "Map name");
            pathToFileEdit = GUI.TextField(new Rect(position.width - 103, 75, 100, 20), pathToFileEdit);

            if (GUI.Button(new Rect(5, 100, 120, 25), "Generate from file"))
            {
                GenerateFromFile();
            }

            if (GUI.Button(new Rect(130, 100, 90, 25), "Save to file"))
            {
                SaveToFile();
            }

            if (GUI.Button(new Rect(5, 130, 120, 25), "Generate Map"))
            {
                GenerateMap();
            }
        }

        private void GenerateFromFile()
        {
            string assetPath = "Assets/Utilities/MapCreator/MapFiles/" + pathToFileEdit + ".txt";

            StreamReader reader = new StreamReader(assetPath);
            string json = reader.ReadToEnd();
            reader.Close();

            MCMapObject mapObj = JsonUtility.FromJson<MCMapObject>(json);

            GameObject map = GameObject.Find("MCMap");
            if (map != null)
            {
                DestroyImmediate(map);
            }

            map = new GameObject("MCMap");
            map.AddComponent<MCMap>();

            int maxWidth = 0;
            int maxHeight = 0;

            foreach (var hex in mapObj.hexes)
            {
                GameObject hexPrefab = mcHexesDict[hex.type.ToString()];

                Vector3 hexPosition = new Vector3(hexWidth * 3 / 4 * hex.y, 0.0f, hexHeight / 2 * (hex.y % 2) + hexHeight * hex.x);
                GameObject h = PrefabUtility.InstantiatePrefab(hexPrefab, map.transform) as GameObject;
                h.transform.position = hexPosition;
                h.name = "MCHex" + hex.x + hex.y;
                h.GetComponent<MCHex>().x = hex.x;
                h.GetComponent<MCHex>().y = hex.y;

                if (hex.hasSpawnPoint)
                {
                    GameObject spawnPoint = Instantiate(GetSpawnPointPrefab(), h.transform);
                    spawnPoint.transform.position = h.transform.position;
                    h.GetComponent<MCHex>().hasSpawnPoint = true;
                }

                if (hex.y > maxWidth)
                {
                    maxWidth = hex.y;
                }

                if (hex.x > maxHeight)
                {
                    maxHeight = hex.x;
                }
            }

            widthEdit = (maxWidth + 1).ToString();
            heightEdit = (maxHeight + 1).ToString();

            PrefabUtility.SaveAsPrefabAssetAndConnect(map, mcHexPrefabFolderPath + "/MCMap.prefab", InteractionMode.AutomatedAction);
        }

        private void SaveToFile()
        {
            GameObject map = GameObject.Find("MCMap");
            MCMapObject mapObj = map.GetComponent<MCMap>().GetMap(pathToFileEdit);
            string json = JsonUtility.ToJson(mapObj);

            string assetPath = "Assets/Utilities/MapCreator/MapFiles/" + pathToFileEdit + ".txt";

            StreamWriter writer = new StreamWriter(assetPath, false);
            writer.Write(json);
            writer.Close();

            AssetDatabase.ImportAsset(assetPath);
        }

        private GameObject GetHexPrefab(string name)
        {
            string hexPrefabFolderPath = "Assets/Prefabs/Hexes";
            string[] hexGUIDs = AssetDatabase.FindAssets("Hex" + name, new[] { hexPrefabFolderPath });
            string hexPath = AssetDatabase.GUIDToAssetPath(hexGUIDs[0]);
            return (GameObject)AssetDatabase.LoadAssetAtPath(hexPath, typeof(GameObject));
        }

        private GameObject GetMCHexPrefab(string name)
        {
            string hexPrefabFolderPath = "Assets/Utilities/MapCreator/Prefabs";
            string[] hexGUIDs = AssetDatabase.FindAssets("MCHex" + name, new[] { hexPrefabFolderPath });
            string hexPath = AssetDatabase.GUIDToAssetPath(hexGUIDs[0]);
            return (GameObject)AssetDatabase.LoadAssetAtPath(hexPath, typeof(GameObject));
        }

        private GameObject GetSpawnPointPrefab()
        {
            string prefabFolderPath = "Assets/Utilities/MapCreator/Prefabs";
            string[] GUIDs = AssetDatabase.FindAssets("MCSpawnPoint", new[] { prefabFolderPath });
            string path = AssetDatabase.GUIDToAssetPath(GUIDs[0]);
            return (GameObject)AssetDatabase.LoadAssetAtPath(path, typeof(GameObject));
        }

        private void GenerateMap()
        {
            string mapFilePath = "Assets/Utilities/MapCreator/MapFiles/" + pathToFileEdit + ".txt";
            string mapPrefabPath = "Assets/Prefabs/Maps/";

            StreamReader reader = new StreamReader(mapFilePath);
            string json = reader.ReadToEnd();
            reader.Close();

            MCMapObject mapObj = JsonUtility.FromJson<MCMapObject>(json);

            GameObject map = GameObject.Find("Map");
            if (map != null)
            {
                DestroyImmediate(map);
            }

            map = new GameObject("Map");
            map.AddComponent<Map>();

            foreach (var hex in mapObj.hexes)
            {
                if (hex.type != 0)
                {
                    GameObject hexPrefab = hexesDict[hex.type.ToString()];

                    Vector3 hexPosition = new Vector3(hexWidth * 3 / 4 * hex.y, 0.0f, hexHeight / 2 * (hex.y % 2) + hexHeight * hex.x);
                    GameObject h = PrefabUtility.InstantiatePrefab(hexPrefab, map.transform) as GameObject;
                    h.transform.position = hexPosition;
                    h.name = "Hex" + hex.x + hex.y;
                    h.GetComponent<Hex>().x = hex.x;
                    h.GetComponent<Hex>().y = hex.y;

                    if (hex.hasSpawnPoint)
                    {
                        if (map.GetComponent<Map>().spawnPoints == null)
                        {
                            map.GetComponent<Map>().spawnPoints = new List<GameObject>();
                        }
                        map.GetComponent<Map>().spawnPoints.Add(h);
                    }
                }
            }

            int height = int.Parse(heightEdit);
            int width = int.Parse(widthEdit);

            for (int i = 0; i < height; i++)
            {
                for (int j = 0; j < width; j++)
                {
                    GameObject targetHex = GameObject.Find("Hex" + i + j).gameObject;

                    GameObject hex0 = null;
                    GameObject hex1 = null;
                    GameObject hex2 = null;
                    GameObject hex3 = null;
                    GameObject hex4 = null;
                    GameObject hex5 = null;

                    if (j % 2 == 0)
                    {
                        if (i < height - 1)
                        {
                            hex0 = GameObject.Find("Hex" + (i + 1) + j).gameObject;
                        }
                        if (j < width - 1)
                        {
                            hex1 = GameObject.Find("Hex" + i + (j + 1)).gameObject;
                        }
                        if (i > 0 && j < width - 1)
                        {
                            hex2 = GameObject.Find("Hex" + (i - 1) + (j + 1)).gameObject;
                        }
                        if (i > 0)
                        {
                            hex3 = GameObject.Find("Hex" + (i - 1) + j).gameObject;
                        }
                        if (i > 0 && j > 0)
                        {
                            hex4 = GameObject.Find("Hex" + (i - 1) + (j - 1)).gameObject;
                        }
                        if (j > 0)
                        {
                            hex5 = GameObject.Find("Hex" + i + (j - 1)).gameObject;
                        }
                    }
                    else
                    {
                        if (i < height - 1)
                        {
                            hex0 = GameObject.Find("Hex" + (i + 1) + j).gameObject;
                        }
                        if (i < height - 1 && j < width - 1)
                        {
                            hex1 = GameObject.Find("Hex" + (i + 1) + (j + 1)).gameObject;
                        }
                        if (j < width - 1)
                        {
                            hex2 = GameObject.Find("Hex" + i + (j + 1)).gameObject;
                        }
                        if (i > 0)
                        {
                            hex3 = GameObject.Find("Hex" + (i - 1) + j).gameObject;
                        }
                        if (j > 0)
                        {
                            hex4 = GameObject.Find("Hex" + i + (j - 1)).gameObject;
                        }
                        if (i < height - 1 && j > 0)
                        {
                            hex5 = GameObject.Find("Hex" + (i + 1) + (j - 1)).gameObject;
                        }
                    }

                    targetHex.GetComponent<Hex>().neighbors = new List<GameObject>();
                    targetHex.GetComponent<Hex>().neighbors.AddRange(new List<GameObject> { hex0, hex1, hex2, hex3, hex4, hex5 });
                }
            }

            PrefabUtility.SaveAsPrefabAssetAndConnect(map, mapPrefabPath + "/" + mapObj.name + ".prefab", InteractionMode.AutomatedAction);

            DestroyImmediate(map);
        }
    }
}
