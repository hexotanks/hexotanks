﻿using UnityEngine;
using UnityEditor;
using System.IO;
using System;
using System.Collections.Generic;

namespace Utilities.TankCreator
{
    public class TankImporterPostprocessor : AssetPostprocessor
    {
        private void OnPostprocessModel(GameObject g)
        {
            //if (g.name.Equals("Gun"))
            //{
            //    g.transform.position = new Vector3(1.0f, 0.0f, 0.0f);
            //    g.AddComponent<TankGun>();
            //}

            //Debug.Log(assetPath);
        }

        private void OnPostprocessMeshHierarchy(GameObject g)
        {
            //Debug.Log(assetPath);
        }

        //static void OnPostprocessAllAssets(string[] importedAssets, string[] deletedAssets, string[] movedAssets, string[] movedFromAssetPaths)
        //{
        //    foreach (string assetPath in importedAssets)
        //    {
        //        if (Path.GetFileName(assetPath).Equals("Body.fbx"))
        //        {
        //            GameObject g = (GameObject)AssetDatabase.LoadAssetAtPath(assetPath, typeof(GameObject));
        //            g.AddComponent<TankBody>();
        //        }
        //    }
        //}

        private void OnPreprocessModel()
        {
            // TANK PARTS
            string objName = Path.GetFileName(assetPath).ToLower();
            if (objName.Equals("gun.fbx") || objName.Equals("body.fbx") || objName.Equals("turret.fbx") || objName.Equals("mantlet.fbx"))
            {
                ModelImporter modelImporter = assetImporter as ModelImporter;
                modelImporter.animationType = ModelImporterAnimationType.None;
            }
        }

        private void OnPreprocessAnimation()
        {
            // TANK PARTS
            string objName = Path.GetFileName(assetPath).ToLower();
            if (objName.Equals("gun.fbx") || objName.Equals("body.fbx") || objName.Equals("turret.fbx") || objName.Equals("mantlet.fbx"))
            {
                ModelImporter modelImporter = assetImporter as ModelImporter;
                modelImporter.importAnimation = false;
            }
        }
    }
}
