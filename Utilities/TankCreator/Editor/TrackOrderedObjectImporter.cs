﻿using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEditor.Experimental.AssetImporters;

namespace Utilities.TankCreator
{
    [ScriptedImporter(1, "orobj")]
    public class TrackOrderedObjectImporter : ScriptedImporter
    {
        public override void OnImportAsset(AssetImportContext ctx)
        {
            string objName = Path.GetFileName(assetPath);

            GameObject mainAsset = new GameObject();

            Dictionary<string, List<string[]>> obj = ParseObj(ctx.assetPath);
            Mesh mesh = ConstructMesh(obj);
            mesh.name = obj["o"][0][0];
            MeshFilter meshFilter = mainAsset.AddComponent<MeshFilter>();
            meshFilter.sharedMesh = mesh;
            ctx.AddObjectToAsset("Mesh", mesh);

            Material[] materials = new Material[mesh.subMeshCount];
            Shader standard = Shader.Find("Standard");
            materials[0] = new Material(standard);
            materials[0].SetFloat("_Glossiness", 0.0f);
            if (objName.Contains("Track_L") || objName.Contains("Track_R"))
            {
                materials[0].name = "Track Material";

                ctx.AddObjectToAsset("Track Material", materials[0]);
            }
            if (objName.Contains("Wheels_L") || objName.Contains("Wheels_R"))
            {
                materials[0].name = "Wheels Material";
                ctx.AddObjectToAsset("Wheels Material", materials[0]);
            }
            MeshRenderer renderer = mainAsset.AddComponent<MeshRenderer>();
            renderer.materials = materials;

            ctx.AddObjectToAsset("main obj", mainAsset);
            ctx.SetMainObject(mainAsset);
        }

        private Mesh ConstructMesh(Dictionary<string, List<string[]>> data)
        {
            Mesh result;
            List<string[]> f = data["f"];
            result = new Mesh();
            result.subMeshCount = 1;

            List<string[]> v = data["v"];
            Vector3[] vertices = new Vector3[v.Count];
            for (int i = 0; i < v.Count; i++)
            {
                string[] raw = v[i];

                float x = float.Parse(raw[0]);
                float y = float.Parse(raw[1]);
                float z = float.Parse(raw[2]);
                vertices[i] = new Vector3(x, y, z);
            }

            List<string[]> vn = data["vn"];
            Vector3[] normals = new Vector3[vn.Count];
            for (int i = 0; i < vn.Count; i++)
            {
                string[] raw = vn[i];

                float x = float.Parse(raw[0]);
                float y = float.Parse(raw[1]);
                float z = float.Parse(raw[2]);
                normals[i] = new Vector3(x, y, z);
            }

            int[] triangleIndices = new int[f.Count * 3];
            for (int i = 0; i < f.Count; i++)
            {
                string[] raw = f[i];
                string s1 = raw[0];
                string s2 = raw[1];
                string s3 = raw[2];
                int v1 = int.Parse(s1);
                int v2 = int.Parse(s2);
                int v3 = int.Parse(s3);
                triangleIndices[i * 3] = v1;
                triangleIndices[i * 3 + 1] = v2;
                triangleIndices[i * 3 + 2] = v3;
            }


            result.SetVertices(vertices);
            result.SetNormals(normals);
            result.SetIndices(triangleIndices, MeshTopology.Triangles, 0, false);

            result.RecalculateBounds();
            result.RecalculateNormals();
            return result;
        }

        private Dictionary<string, List<string[]>> ParseObj(string filepath)
        {
            Dictionary<string, List<string[]>> result = new Dictionary<string, List<string[]>>();
            List<string[]> o = new List<string[]>();
            List<string[]> v = new List<string[]>();
            List<string[]> vn = new List<string[]>();
            List<string[]> f = new List<string[]>();

            using (StreamReader sr = File.OpenText(filepath))
            {
                string s = string.Empty;
                string[] line;
                while ((s = sr.ReadLine()) != null)
                {
                    if (s.StartsWith("o "))
                    {
                        line = s.Split(' ');
                        string[] lineData = { line[1] };
                        o.Add(lineData);
                    }
                    else if (s.StartsWith("v "))
                    {
                        line = s.Split(' ');
                        string[] lineData = { line[1], line[2], line[3] };
                        v.Add(lineData);
                    }
                    else if (s.StartsWith("vn "))
                    {
                        line = s.Split(' ');
                        string[] lineData = { line[1], line[2], line[3] };
                        vn.Add(lineData);
                    }
                    else if (s.StartsWith("f "))
                    {
                        line = s.Split(' ');
                        string[] lineData = { line[1], line[2], line[3] };
                        f.Add(lineData);
                    }
                }
            }

            result.Add("o", o);
            result.Add("v", v);
            result.Add("vn", vn);
            result.Add("f", f);
            return result;
        }
    }
}
