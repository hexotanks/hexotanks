﻿using UnityEngine;
using UnityEditor;
using System.IO;
using System.Collections.Generic;

namespace Utilities.TankCreator
{
    public class TankImporter : EditorWindow
    {
        private static string[] tanksList = new string[] { "" };
        private static string exportedTanksFolder = @"D:\project_tanks\test\models\tanks\exported\tanks";
        private static string exportedTanksSubfoldersPath = @"D:\project_tanks\test\models\tanks\exported\tanks\";

        [MenuItem("Import/Tank")]
        static void Init()
        {
            TankImporter window = (TankImporter)EditorWindow.GetWindow(typeof(TankImporter));
            window.titleContent.text = "Tank Importer";
            window.Show();
        }

        void OnGUI()
        {
            GetTanksList();

            GUILayout.Label("Import Options", EditorStyles.boldLabel);

            int selectedTankId = 0;
            selectedTankId = EditorGUILayout.Popup("Tank Name", selectedTankId, tanksList);
            string selectedTankName = tanksList[selectedTankId];

            if (GUI.Button(new Rect(position.width - 113, 40, 110, 25), "Import/Reimport"))
            {
                string pathToAssets = Directory.GetParent(Application.dataPath).FullName;

                string[] folders = AssetDatabase.GetSubFolders("Assets/Objects/Tanks");
                foreach (string folder in folders)
                {
                    string tankFolderName = new DirectoryInfo(folder).Name;
                    if (tankFolderName.Equals(selectedTankName))
                    {
                        AssetDatabase.DeleteAsset(folder);
                    }
                }

                // IMPORT

                AssetDatabase.CreateFolder("Assets/Objects/Tanks", selectedTankName);
                AssetDatabase.CreateFolder("Assets/Objects/Tanks/" + selectedTankName, "specs");

                string specFolderPath = Path.Combine(exportedTanksFolder, selectedTankName, "specs");
                string tankFolderPath = Path.Combine(exportedTanksFolder, selectedTankName);
                string targetTankFolderPath = "Assets/Objects/Tanks/" + selectedTankName;
                string targetTankSpecFolderPath = targetTankFolderPath + "/specs";

                // IMPORT SPECS

                foreach (var file in Directory.GetFiles(specFolderPath))
                {
                    File.Copy(file, Path.Combine(targetTankSpecFolderPath, Path.GetFileName(file)), true);
                }

                AssetDatabase.Refresh();

                // IMPORT TANK

                foreach (var file in Directory.GetFiles(tankFolderPath))
                {
                    File.Copy(file, Path.Combine(targetTankFolderPath, Path.GetFileName(file)), true);
                }

                AssetDatabase.Refresh();
            }

            if (GUI.Button(new Rect(3, 40, 110, 25), "Create Prefab"))
            {
                string specFolderPath = "Assets/Objects/Tanks/" + selectedTankName + "/specs";
                string tankFolderPath = "Assets/Objects/Tanks/" + selectedTankName;
                string shellPrefabFolderPath = "Assets/Prefabs/Shells";
                string tankPrefabFolderPath = "Assets/Prefabs/Tanks";
                string tankMaterialsFolderPath = "Assets/Materials/Tanks";

                AssetDatabase.DeleteAsset(tankPrefabFolderPath + "/" + selectedTankName + ".prefab");

                TextAsset tankSpec = (TextAsset)AssetDatabase.LoadAssetAtPath(specFolderPath + "/Tank_spec.txt", typeof(TextAsset));
                List<string> specData = tankSpec.ReadLines();
                TankData tankData = new SpecParser(specData[0]).GetTankData();

                Material trackMaterial = (Material)AssetDatabase.LoadAssetAtPath(tankMaterialsFolderPath + "/mat_Tracks.mat", typeof(Material));
                Material bodyNationMaterial = (Material)AssetDatabase.LoadAssetAtPath(tankMaterialsFolderPath + $"/mat_{tankData.nation}.mat", typeof(Material));
                Material dirtyBodyNationMaterial = (Material)AssetDatabase.LoadAssetAtPath(tankMaterialsFolderPath + $"/mat_{tankData.nation}_dirty.mat", typeof(Material));

                GameObject root = new GameObject(selectedTankName);
                Tank tankComponent = root.AddComponent<Tank>();

                GameObject bodyAsset = (GameObject)AssetDatabase.LoadAssetAtPath(tankFolderPath + "/Body.fbx", typeof(GameObject));
                GameObject body = Instantiate(bodyAsset, root.transform);
                body.name = "Body";
                body.AddComponent<TankBody>();
                body.GetComponent<MeshRenderer>().sharedMaterials = new Material[] { dirtyBodyNationMaterial, bodyNationMaterial };

                GameObject turretAsset = (GameObject)AssetDatabase.LoadAssetAtPath(tankFolderPath + "/Turret.fbx", typeof(GameObject));
                GameObject turret = Instantiate(turretAsset, root.transform);
                turret.name = "Turret";
                turret.AddComponent<TankTurret>();
                turret.GetComponent<MeshRenderer>().sharedMaterials = new Material[] { bodyNationMaterial };

                GameObject mantletAsset = (GameObject)AssetDatabase.LoadAssetAtPath(tankFolderPath + "/Mantlet.fbx", typeof(GameObject));
                GameObject mantlet = Instantiate(mantletAsset, root.transform);
                mantlet.transform.parent = turret.transform;
                mantlet.name = "Mantlet";
                mantlet.AddComponent<TankMantlet>();
                mantlet.GetComponent<MeshRenderer>().sharedMaterials = new Material[] { bodyNationMaterial };

                GameObject gunAsset = (GameObject)AssetDatabase.LoadAssetAtPath(tankFolderPath + "/Gun.fbx", typeof(GameObject));
                GameObject gun = Instantiate(gunAsset, root.transform);
                gun.transform.parent = mantlet.transform;
                gun.name = "Gun";
                TankGun tankGunComponent = gun.AddComponent<TankGun>();
                gun.GetComponent<MeshRenderer>().sharedMaterials = new Material[] { bodyNationMaterial };

                GameObject shootPoint = new GameObject("Shoot_Point");
                Vector3 v = gun.transform.rotation * tankData.shootPoint;
                shootPoint.transform.position = v;
                shootPoint.transform.rotation = gun.transform.rotation;
                shootPoint.transform.parent = gun.transform;

                string[] shellGUIDs = AssetDatabase.FindAssets("Shell_" + tankData.caliber, new[] { shellPrefabFolderPath });
                string shellPath = AssetDatabase.GUIDToAssetPath(shellGUIDs[0]);
                GameObject shell = (GameObject)AssetDatabase.LoadAssetAtPath(shellPath, typeof(GameObject));
                tankGunComponent.shell = shell;

                GameObject trackCurveLAsset = (GameObject)AssetDatabase.LoadAssetAtPath(tankFolderPath + "/TrackCurve_L.curve", typeof(GameObject));
                GameObject trackCurveL = Instantiate(trackCurveLAsset, root.transform);
                trackCurveL.name = "TrackCurve_L";

                GameObject trackCurveRAsset = (GameObject)AssetDatabase.LoadAssetAtPath(tankFolderPath + "/TrackCurve_R.curve", typeof(GameObject));
                GameObject trackCurveR = Instantiate(trackCurveRAsset, root.transform);
                trackCurveR.name = "TrackCurve_R";

                GameObject trackLAsset = (GameObject)AssetDatabase.LoadAssetAtPath(tankFolderPath + "/Track_L.orobj", typeof(GameObject));
                GameObject trackL = Instantiate(trackLAsset, root.transform);
                trackL.name = "Track_L";
                TankTrack tankTrackLComponent = trackL.AddComponent<TankTrack>();
                TextAsset tankTrackLSpec = (TextAsset)AssetDatabase.LoadAssetAtPath(specFolderPath + "/Track_L_spec.txt", typeof(TextAsset));
                tankTrackLComponent.spec = tankTrackLSpec;
                tankTrackLComponent.curve = trackCurveL;
                trackL.GetComponent<MeshRenderer>().sharedMaterials = new Material[] { trackMaterial };

                GameObject trackRAsset = (GameObject)AssetDatabase.LoadAssetAtPath(tankFolderPath + "/Track_R.orobj", typeof(GameObject));
                GameObject trackR = Instantiate(trackRAsset, root.transform);
                trackR.name = "Track_R";
                TankTrack tankTrackRComponent = trackR.AddComponent<TankTrack>();
                TextAsset tankTrackRSpec = (TextAsset)AssetDatabase.LoadAssetAtPath(specFolderPath + "/Track_R_spec.txt", typeof(TextAsset));
                tankTrackRComponent.spec = tankTrackRSpec;
                tankTrackRComponent.curve = trackCurveR;
                trackR.GetComponent<MeshRenderer>().sharedMaterials = new Material[] { trackMaterial };

                GameObject wheelsLAsset = (GameObject)AssetDatabase.LoadAssetAtPath(tankFolderPath + "/Wheels_L.orobj", typeof(GameObject));
                GameObject wheelsL = Instantiate(wheelsLAsset, root.transform);
                wheelsL.name = "Wheels_L";
                TankWheels tankWheelsLComponent = wheelsL.AddComponent<TankWheels>();
                TextAsset tankWheelsLSpec = (TextAsset)AssetDatabase.LoadAssetAtPath(specFolderPath + "/Wheels_L_spec.txt", typeof(TextAsset));
                tankWheelsLComponent.spec = tankWheelsLSpec;
                wheelsL.GetComponent<MeshRenderer>().sharedMaterials = new Material[] { dirtyBodyNationMaterial };

                GameObject wheelsRAsset = (GameObject)AssetDatabase.LoadAssetAtPath(tankFolderPath + "/Wheels_R.orobj", typeof(GameObject));
                GameObject wheelsR = Instantiate(wheelsRAsset, root.transform);
                wheelsR.name = "Wheels_R";
                TankWheels tankWheelsRComponent = wheelsR.AddComponent<TankWheels>();
                TextAsset tankWheelsRSpec = (TextAsset)AssetDatabase.LoadAssetAtPath(specFolderPath + "/Wheels_R_spec.txt", typeof(TextAsset));
                tankWheelsRComponent.spec = tankWheelsRSpec;
                wheelsR.GetComponent<MeshRenderer>().sharedMaterials = new Material[] { dirtyBodyNationMaterial };

                PrefabUtility.SaveAsPrefabAssetAndConnect(root, tankPrefabFolderPath + "/" + selectedTankName + ".prefab", InteractionMode.AutomatedAction);
            }
        }

        private static void GetTanksList()
        {
            string[] tanksFoldersList = Directory.GetDirectories(exportedTanksFolder);
            tanksList = new string[tanksFoldersList.Length];
            for (int i = 0; i < tanksFoldersList.Length; i++)
            {
                int index = tanksFoldersList[i].IndexOf(exportedTanksSubfoldersPath);
                string tankName = (index < 0) ? tanksFoldersList[i] : tanksFoldersList[i].Remove(index, exportedTanksSubfoldersPath.Length);
                tanksList[i] = tankName;
            }
        }
    }
}
