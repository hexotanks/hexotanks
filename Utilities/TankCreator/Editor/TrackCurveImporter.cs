﻿using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEditor.Experimental.AssetImporters;

namespace Utilities.TankCreator
{
    [ScriptedImporter(1, "curve")]
    public class TrackCurveImporter : ScriptedImporter
    {
        public override void OnImportAsset(AssetImportContext ctx)
        {
            GameObject mainAsset = new GameObject();

            Dictionary<string, List<string[]>> obj = ParseObj(ctx.assetPath);
            Mesh mesh = ConstructMesh(obj);
            mesh.name = obj["o"][0][0];
            MeshFilter meshFilter = mainAsset.AddComponent<MeshFilter>();
            meshFilter.sharedMesh = mesh;
            ctx.AddObjectToAsset("Mesh", mesh);

            Material[] materials = new Material[mesh.subMeshCount];
            Shader standard = Shader.Find("Standard");
            materials[0] = new Material(standard);
            materials[0].name = "Track Curve Material";
            ctx.AddObjectToAsset("Track Curve Material", materials[0]);
            MeshRenderer renderer = mainAsset.AddComponent<MeshRenderer>();
            renderer.enabled = false;
            renderer.materials = materials;

            ctx.AddObjectToAsset("main obj", mainAsset);
            ctx.SetMainObject(mainAsset);
        }

        private Mesh ConstructMesh(Dictionary<string, List<string[]>> data)
        {
            Mesh result;
            List<string[]> e = data["e"];
            result = new Mesh();
            result.subMeshCount = 1;

            List<string[]> v = data["v"];
            Vector3[] vertices = new Vector3[v.Count];
            for (int i = 0; i < v.Count; i++)
            {
                string[] raw = v[i];

                float x = float.Parse(raw[0]);
                float y = float.Parse(raw[1]);
                float z = float.Parse(raw[2]);
                vertices[i] = new Vector3(x, y, z);
            }
            result.vertices = vertices;

            int[] edgeIndices = new int[e.Count * 2];
            for (int i = 0; i < e.Count; i++)
            {
                string[] raw = e[i];
                int v1 = int.Parse(raw[0]) - 1;
                int v2 = int.Parse(raw[1]) - 1;
                edgeIndices[i * 2] = v1;
                edgeIndices[i * 2 + 1] = v2;
            }
            result.SetIndices(edgeIndices, MeshTopology.Lines, 0, false);

            result.RecalculateBounds();
            return result;
        }

        private Dictionary<string, List<string[]>> ParseObj(string filepath)
        {
            Dictionary<string, List<string[]>> result = new Dictionary<string, List<string[]>>();
            List<string[]> o = new List<string[]>();
            List<string[]> v = new List<string[]>();
            List<string[]> e = new List<string[]>();

            using (StreamReader sr = File.OpenText(filepath))
            {
                string s = string.Empty;
                string[] line;
                while ((s = sr.ReadLine()) != null)
                {
                    if (s.StartsWith("o "))
                    {
                        line = s.Split(' ');
                        string[] lineData = { line[1] };
                        o.Add(lineData);
                    }
                    else if (s.StartsWith("v "))
                    {
                        line = s.Split(' ');
                        string[] lineData = { line[1], line[2], line[3] };
                        v.Add(lineData);
                    }
                    else if (s.StartsWith("l "))
                    {
                        line = s.Split(' ');
                        string[] lineData = { line[1], line[2] };
                        e.Add(lineData);
                    }
                }
            }

            result.Add("o", o);
            result.Add("v", v);
            result.Add("e", e);
            return result;
        }
    }
}
